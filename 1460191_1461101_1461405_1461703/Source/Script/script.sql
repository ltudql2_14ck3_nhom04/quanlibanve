CREATE DATABASE [quanlibanve]
GO
USE [quanlibanve]
GO
/****** Object:  Table [dbo].[HinhThuc]    Script Date: 06/25/2017 11:42:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HinhThuc](
	[LoaiHT] [varchar](10) NOT NULL,
	[TenHT] [nvarchar](50) NULL,
 CONSTRAINT [PK_HinhThuc] PRIMARY KEY CLUSTERED 
(
	[LoaiHT] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[HinhThuc] ([LoaiHT], [TenHT]) VALUES (N'H01', N'Đi xe')
INSERT [dbo].[HinhThuc] ([LoaiHT], [TenHT]) VALUES (N'H02', N'Gửi Hàng')
INSERT [dbo].[HinhThuc] ([LoaiHT], [TenHT]) VALUES (N'H03', N'Nhận Hàng')
/****** Object:  Table [dbo].[XeKhach]    Script Date: 06/25/2017 11:42:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[XeKhach](
	[SoXe] [varchar](15) NOT NULL,
	[HangSX] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_XeKhach] PRIMARY KEY CLUSTERED 
(
	[SoXe] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[XeKhach] ([SoXe], [HangSX]) VALUES (N'10B4-32435', N'SMART')
INSERT [dbo].[XeKhach] ([SoXe], [HangSX]) VALUES (N'12B3-00089', N'CHEVROLET')
INSERT [dbo].[XeKhach] ([SoXe], [HangSX]) VALUES (N'20B7-43211', N'BENTLY')
INSERT [dbo].[XeKhach] ([SoXe], [HangSX]) VALUES (N'23B1-11553', N'JEEP')
INSERT [dbo].[XeKhach] ([SoXe], [HangSX]) VALUES (N'25B4-47832', N'JACUAR')
INSERT [dbo].[XeKhach] ([SoXe], [HangSX]) VALUES (N'34B5-00127', N'CODING')
INSERT [dbo].[XeKhach] ([SoXe], [HangSX]) VALUES (N'56B2-89110', N'SUZUKI')
INSERT [dbo].[XeKhach] ([SoXe], [HangSX]) VALUES (N'60B1-01582', N'KIA')
INSERT [dbo].[XeKhach] ([SoXe], [HangSX]) VALUES (N'62B2-87987', N'HUYNDAI')
INSERT [dbo].[XeKhach] ([SoXe], [HangSX]) VALUES (N'63B1-00001', N'TOYOTA')
INSERT [dbo].[XeKhach] ([SoXe], [HangSX]) VALUES (N'65B3-23874', N'MECREDES-BENZ')
INSERT [dbo].[XeKhach] ([SoXe], [HangSX]) VALUES (N'71B6-10034', N'HINO')
INSERT [dbo].[XeKhach] ([SoXe], [HangSX]) VALUES (N'74B3-99331', N'GMC')
INSERT [dbo].[XeKhach] ([SoXe], [HangSX]) VALUES (N'78B3-22115', N'OPEL')
INSERT [dbo].[XeKhach] ([SoXe], [HangSX]) VALUES (N'85B1-00003', N'YAMAHA')
INSERT [dbo].[XeKhach] ([SoXe], [HangSX]) VALUES (N'86B2-00011', N'AUDI')
INSERT [dbo].[XeKhach] ([SoXe], [HangSX]) VALUES (N'88B7-11703', N'LEXUS')
INSERT [dbo].[XeKhach] ([SoXe], [HangSX]) VALUES (N'89B2-77862', N'HONDA')
INSERT [dbo].[XeKhach] ([SoXe], [HangSX]) VALUES (N'89B5-67874', N'BMW')
INSERT [dbo].[XeKhach] ([SoXe], [HangSX]) VALUES (N'90B1-44351', N'FUSO')
/****** Object:  Table [dbo].[LoaiTK]    Script Date: 06/25/2017 11:42:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LoaiTK](
	[MaLoaiTK] [varchar](10) NOT NULL,
	[TenLoai] [nvarchar](50) NULL,
 CONSTRAINT [PK_LoaiTK] PRIMARY KEY CLUSTERED 
(
	[MaLoaiTK] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[LoaiTK] ([MaLoaiTK], [TenLoai]) VALUES (N'1', N'Quản Lí')
INSERT [dbo].[LoaiTK] ([MaLoaiTK], [TenLoai]) VALUES (N'2', N'Nhân Viên')
/****** Object:  Table [dbo].[LoaiNhanVien]    Script Date: 06/25/2017 11:42:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LoaiNhanVien](
	[MaLoaiNV] [varchar](10) NOT NULL,
	[TenLoai] [nvarchar](50) NULL,
 CONSTRAINT [PK_LoaiNhanVien] PRIMARY KEY CLUSTERED 
(
	[MaLoaiNV] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[LoaiNhanVien] ([MaLoaiNV], [TenLoai]) VALUES (N'LNV001', N'Quản lí')
INSERT [dbo].[LoaiNhanVien] ([MaLoaiNV], [TenLoai]) VALUES (N'LNV002', N'Bán vé')
INSERT [dbo].[LoaiNhanVien] ([MaLoaiNV], [TenLoai]) VALUES (N'LNV003', N'Tài xế')
INSERT [dbo].[LoaiNhanVien] ([MaLoaiNV], [TenLoai]) VALUES (N'LNV004', N'Phụ xe')
/****** Object:  Table [dbo].[TuyenXe]    Script Date: 06/25/2017 11:42:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TuyenXe](
	[MaTuyen] [varchar](10) NOT NULL,
	[DiaDiemDi] [nvarchar](50) NULL,
	[DiaDiemDen] [nvarchar](50) NULL,
	[GioDi] [time](7) NULL,
	[GioDen] [time](7) NULL,
 CONSTRAINT [PK_TuyenXe] PRIMARY KEY CLUSTERED 
(
	[MaTuyen] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[TuyenXe] ([MaTuyen], [DiaDiemDi], [DiaDiemDen], [GioDi], [GioDen]) VALUES (N'T001', N'TPHCM', N'Nha Trang', CAST(0x0700709A4A320000 AS Time), CAST(0x070010ACD1530000 AS Time))
INSERT [dbo].[TuyenXe] ([MaTuyen], [DiaDiemDi], [DiaDiemDen], [GioDi], [GioDen]) VALUES (N'T002', N'Vũng Tàu', N'Ninh Thuận', CAST(0x0700D85EAC3A0000 AS Time), CAST(0x070050CFDF960000 AS Time))
INSERT [dbo].[TuyenXe] ([MaTuyen], [DiaDiemDi], [DiaDiemDen], [GioDi], [GioDen]) VALUES (N'T003', N'TPHCM', N'Tiền Giang', CAST(0x0700E03495640000 AS Time), CAST(0x07001882BA7D0000 AS Time))
INSERT [dbo].[TuyenXe] ([MaTuyen], [DiaDiemDi], [DiaDiemDen], [GioDi], [GioDen]) VALUES (N'T004', N'Cà Mau', N'Bình Thuận', CAST(0x07007870335C0000 AS Time), CAST(0x070058A5C8C00000 AS Time))
INSERT [dbo].[TuyenXe] ([MaTuyen], [DiaDiemDi], [DiaDiemDen], [GioDi], [GioDen]) VALUES (N'T005', N'TPHCM', N'Đồng Nai', CAST(0x070040230E430000 AS Time), CAST(0x070010ACD1530000 AS Time))
INSERT [dbo].[TuyenXe] ([MaTuyen], [DiaDiemDi], [DiaDiemDen], [GioDi], [GioDen]) VALUES (N'T006', N'Vũng Tàu', N'TPHCM', CAST(0x070068C461080000 AS Time), CAST(0x0700384D25190000 AS Time))
INSERT [dbo].[TuyenXe] ([MaTuyen], [DiaDiemDi], [DiaDiemDen], [GioDi], [GioDen]) VALUES (N'T007', N'Vĩnh Long', N'TPHCM', CAST(0x0700384D25190000 AS Time), CAST(0x0700D85EAC3A0000 AS Time))
INSERT [dbo].[TuyenXe] ([MaTuyen], [DiaDiemDi], [DiaDiemDen], [GioDi], [GioDen]) VALUES (N'T008', N'An Giang', N'Bình Thuận', CAST(0x0700D088C3100000 AS Time), CAST(0x070040230E430000 AS Time))
INSERT [dbo].[TuyenXe] ([MaTuyen], [DiaDiemDi], [DiaDiemDen], [GioDi], [GioDen]) VALUES (N'T009', N'Hà Nội', N'TPHCM', CAST(0x070068C461080000 AS Time), CAST(0x0700F0E066B80000 AS Time))
INSERT [dbo].[TuyenXe] ([MaTuyen], [DiaDiemDi], [DiaDiemDen], [GioDi], [GioDen]) VALUES (N'T010', N'Đà Nẵng', N'Ninh Bình', CAST(0x070008D6E8290000 AS Time), CAST(0x0700E03495640000 AS Time))
INSERT [dbo].[TuyenXe] ([MaTuyen], [DiaDiemDi], [DiaDiemDen], [GioDi], [GioDen]) VALUES (N'T011', N'An Giang', N'Tiền Giang', CAST(0x07002058A3A70000 AS Time), CAST(0x0700F0E066B80000 AS Time))
INSERT [dbo].[TuyenXe] ([MaTuyen], [DiaDiemDi], [DiaDiemDen], [GioDi], [GioDen]) VALUES (N'T012', N'Cần Thơ', N'Vũng Tàu', CAST(0x0700A8E76F4B0000 AS Time), CAST(0x070080461C860000 AS Time))
INSERT [dbo].[TuyenXe] ([MaTuyen], [DiaDiemDi], [DiaDiemDen], [GioDi], [GioDen]) VALUES (N'T013', N'Tiền Giang', N'Vĩnh Long', CAST(0x0700D088C3100000 AS Time), CAST(0x0700384D25190000 AS Time))
INSERT [dbo].[TuyenXe] ([MaTuyen], [DiaDiemDi], [DiaDiemDen], [GioDi], [GioDen]) VALUES (N'T014', N'Ninh Thuận', N'Nha Trang', CAST(0x0700384D25190000 AS Time), CAST(0x0700709A4A320000 AS Time))
INSERT [dbo].[TuyenXe] ([MaTuyen], [DiaDiemDi], [DiaDiemDen], [GioDi], [GioDen]) VALUES (N'T015', N'Nha Trang', N'Ninh Thuận', CAST(0x0700709A4A320000 AS Time), CAST(0x0700A8E76F4B0000 AS Time))
INSERT [dbo].[TuyenXe] ([MaTuyen], [DiaDiemDi], [DiaDiemDen], [GioDi], [GioDen]) VALUES (N'T016', N'Quảng Nam', N'Huế', CAST(0x0700D088C3100000 AS Time), CAST(0x0700B893419F0000 AS Time))
INSERT [dbo].[TuyenXe] ([MaTuyen], [DiaDiemDi], [DiaDiemDen], [GioDi], [GioDen]) VALUES (N'T017', N'SaPa', N'Phú Yên', CAST(0x070050CFDF960000 AS Time), CAST(0x070068C461080000 AS Time))
INSERT [dbo].[TuyenXe] ([MaTuyen], [DiaDiemDi], [DiaDiemDen], [GioDi], [GioDen]) VALUES (N'T018', N'Cần Giờ', N'Long An', CAST(0x070048F9F66C0000 AS Time), CAST(0x070050CFDF960000 AS Time))
INSERT [dbo].[TuyenXe] ([MaTuyen], [DiaDiemDi], [DiaDiemDen], [GioDi], [GioDen]) VALUES (N'T019', N'Long An', N'Bình Định', CAST(0x070080461C860000 AS Time), CAST(0x070058A5C8C00000 AS Time))
INSERT [dbo].[TuyenXe] ([MaTuyen], [DiaDiemDi], [DiaDiemDen], [GioDi], [GioDen]) VALUES (N'T020', N'Vũng Tàu', N'Quảng Trị', CAST(0x070068C461080000 AS Time), CAST(0x0700F0E066B80000 AS Time))
/****** Object:  Table [dbo].[TaiKhoan]    Script Date: 06/25/2017 11:42:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TaiKhoan](
	[TenDN] [varchar](50) NOT NULL,
	[MatKhau] [varchar](10) NOT NULL,
	[LoaiTK] [varchar](10) NULL,
 CONSTRAINT [PK_TaiKhoan] PRIMARY KEY CLUSTERED 
(
	[TenDN] ASC,
	[MatKhau] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[TaiKhoan] ([TenDN], [MatKhau], [LoaiTK]) VALUES (N'domaitruc', N'170322', NULL)
INSERT [dbo].[TaiKhoan] ([TenDN], [MatKhau], [LoaiTK]) VALUES (N'duonghoanglan', N'887823    ', NULL)
INSERT [dbo].[TaiKhoan] ([TenDN], [MatKhau], [LoaiTK]) VALUES (N'huynhvantri', N'120300', NULL)
INSERT [dbo].[TaiKhoan] ([TenDN], [MatKhau], [LoaiTK]) VALUES (N'lengoclinh', N'434312', NULL)
INSERT [dbo].[TaiKhoan] ([TenDN], [MatKhau], [LoaiTK]) VALUES (N'nguyenminhchau', N'332349', NULL)
INSERT [dbo].[TaiKhoan] ([TenDN], [MatKhau], [LoaiTK]) VALUES (N'nguyenminhuyen', N'051122', N'1')
INSERT [dbo].[TaiKhoan] ([TenDN], [MatKhau], [LoaiTK]) VALUES (N'nguyenngochuong', N'332201', N'2')
INSERT [dbo].[TaiKhoan] ([TenDN], [MatKhau], [LoaiTK]) VALUES (N'nguyenthikhanh', N'140522', N'1')
INSERT [dbo].[TaiKhoan] ([TenDN], [MatKhau], [LoaiTK]) VALUES (N'nguyenvanphong', N'170214', N'2')
INSERT [dbo].[TaiKhoan] ([TenDN], [MatKhau], [LoaiTK]) VALUES (N'nguyenxuanhoang', N'232323', N'2')
INSERT [dbo].[TaiKhoan] ([TenDN], [MatKhau], [LoaiTK]) VALUES (N'nhanvien', N'000000', N'2')
INSERT [dbo].[TaiKhoan] ([TenDN], [MatKhau], [LoaiTK]) VALUES (N'phamhoangdung', N'019188', N'2')
INSERT [dbo].[TaiKhoan] ([TenDN], [MatKhau], [LoaiTK]) VALUES (N'phamhuuthanh', N'091414', N'2')
INSERT [dbo].[TaiKhoan] ([TenDN], [MatKhau], [LoaiTK]) VALUES (N'phamninhxuan', N'546777', N'2')
INSERT [dbo].[TaiKhoan] ([TenDN], [MatKhau], [LoaiTK]) VALUES (N'phamngocmai', N'908989', N'2')
INSERT [dbo].[TaiKhoan] ([TenDN], [MatKhau], [LoaiTK]) VALUES (N'phamngoctuyet', N'110199', N'2')
INSERT [dbo].[TaiKhoan] ([TenDN], [MatKhau], [LoaiTK]) VALUES (N'quanli', N'000000', N'1')
INSERT [dbo].[TaiKhoan] ([TenDN], [MatKhau], [LoaiTK]) VALUES (N'tranminhhoang', N'545567', N'2')
INSERT [dbo].[TaiKhoan] ([TenDN], [MatKhau], [LoaiTK]) VALUES (N'tranminhthien', N'768890', N'1')
INSERT [dbo].[TaiKhoan] ([TenDN], [MatKhau], [LoaiTK]) VALUES (N'tranthiennhan', N'321100', N'2')
INSERT [dbo].[TaiKhoan] ([TenDN], [MatKhau], [LoaiTK]) VALUES (N'vominhhuyen', N'120911', N'2')
INSERT [dbo].[TaiKhoan] ([TenDN], [MatKhau], [LoaiTK]) VALUES (N'vovantrung', N'110011', N'2')
/****** Object:  Table [dbo].[PhanCong]    Script Date: 06/25/2017 11:42:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PhanCong](
	[MaNV] [varchar](10) NULL,
	[NgayDi] [date] NULL,
	[MaTuyen] [varchar](10) NOT NULL,
	[MaPX] [varchar](10) NULL,
 CONSTRAINT [PK_PhanCong] PRIMARY KEY CLUSTERED 
(
	[MaTuyen] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[PhanCong] ([MaNV], [NgayDi], [MaTuyen], [MaPX]) VALUES (N'NV003', CAST(0x5F3B0B00 AS Date), N'T001', N'NV004')
INSERT [dbo].[PhanCong] ([MaNV], [NgayDi], [MaTuyen], [MaPX]) VALUES (N'NV006', CAST(0x5F3B0B00 AS Date), N'T002', N'NV008')
INSERT [dbo].[PhanCong] ([MaNV], [NgayDi], [MaTuyen], [MaPX]) VALUES (N'NV007', CAST(0x623B0B00 AS Date), N'T003', N'NV011')
INSERT [dbo].[PhanCong] ([MaNV], [NgayDi], [MaTuyen], [MaPX]) VALUES (N'NV010', CAST(0x643B0B00 AS Date), N'T005', N'NV014')
/****** Object:  Table [dbo].[NhanVien]    Script Date: 06/25/2017 11:42:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NhanVien](
	[MaNV] [varchar](10) NOT NULL,
	[TenNV] [nvarchar](50) NULL,
	[NgaySinh] [date] NULL,
	[CMND] [varchar](15) NULL,
	[DiaChi] [nvarchar](50) NULL,
	[SDT] [varchar](15) NULL,
	[LoaiNV] [varchar](10) NULL,
	[BiXoa] [varchar](10) NULL,
 CONSTRAINT [PK_NhanVien] PRIMARY KEY CLUSTERED 
(
	[MaNV] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[NhanVien] ([MaNV], [TenNV], [NgaySinh], [CMND], [DiaChi], [SDT], [LoaiNV], [BiXoa]) VALUES (N'NV001', N'Dương Hoàng Lan', CAST(0x831F0B00 AS Date), N'312011789', N'14 Võ Văn Kiệt Phường 5 Quận 5', N'0989812365', N'LNV001', N'0')
INSERT [dbo].[NhanVien] ([MaNV], [TenNV], [NgaySinh], [CMND], [DiaChi], [SDT], [LoaiNV], [BiXoa]) VALUES (N'NV002', N'Đỗ Mai Trúc', CAST(0x7D220B00 AS Date), N'201124534', N'34 Nguyễn Đình Chính Phường 17 Quận Phú Nhuận', N'01232656871', N'LNV002', N'0')
INSERT [dbo].[NhanVien] ([MaNV], [TenNV], [NgaySinh], [CMND], [DiaChi], [SDT], [LoaiNV], [BiXoa]) VALUES (N'NV003', N'Huỳnh Văn Trí', CAST(0x2E240B00 AS Date), N'454567761', N'12 Trần Bình Trọng', N'0971234567', N'LNV003', N'0')
INSERT [dbo].[NhanVien] ([MaNV], [TenNV], [NgaySinh], [CMND], [DiaChi], [SDT], [LoaiNV], [BiXoa]) VALUES (N'NV004', N'Lê Ngọc Linh', CAST(0x741E0B00 AS Date), N'232327678', N'127 Nguyễn Văn Linh Phường 7 Quận 7', N'01298787837', N'LNV004', N'0')
INSERT [dbo].[NhanVien] ([MaNV], [TenNV], [NgaySinh], [CMND], [DiaChi], [SDT], [LoaiNV], [BiXoa]) VALUES (N'NV005', N'Nguyễn Minh Châu', CAST(0x321F0B00 AS Date), N'110054879', N'2 Ngô Quyền Phường 2 Quận 6', N'01638987871', N'LNV001', N'0')
INSERT [dbo].[NhanVien] ([MaNV], [TenNV], [NgaySinh], [CMND], [DiaChi], [SDT], [LoaiNV], [BiXoa]) VALUES (N'NV006', N'Nguyễn Minh Uyên', CAST(0x131F0B00 AS Date), N'340098982', N'13 Nguyễn Tri Phương Phường 12 Quận 5', N'0901212672', N'LNV003', N'0')
INSERT [dbo].[NhanVien] ([MaNV], [TenNV], [NgaySinh], [CMND], [DiaChi], [SDT], [LoaiNV], [BiXoa]) VALUES (N'NV007', N'Nguyễn Ngọc Hương', CAST(0x791E0B00 AS Date), N'326767890', N'56 Cách Mạng Tháng Tám Phường 7 Quận 5', N'01648787897', N'LNV003', N'0')
INSERT [dbo].[NhanVien] ([MaNV], [TenNV], [NgaySinh], [CMND], [DiaChi], [SDT], [LoaiNV], [BiXoa]) VALUES (N'NV008', N'Nguyễn Thị Khánh', CAST(0xA91F0B00 AS Date), N'876756451', N'14 Quốc Hương Phường Thảo Điền Quận 2', N'0971212768', N'LNV004', N'0')
INSERT [dbo].[NhanVien] ([MaNV], [TenNV], [NgaySinh], [CMND], [DiaChi], [SDT], [LoaiNV], [BiXoa]) VALUES (N'NV009', N'Nguyễn Văn Phong', CAST(0x7A1F0B00 AS Date), N'893465788', N'245 Xô Viết Nghệ Tĩnh Phường 17 Quận Bình Thạnh', N'01239878879', N'LNV002', N'0')
INSERT [dbo].[NhanVien] ([MaNV], [TenNV], [NgaySinh], [CMND], [DiaChi], [SDT], [LoaiNV], [BiXoa]) VALUES (N'NV010', N'Nguyễn Xuân Hoàng', CAST(0x951E0B00 AS Date), N'008985655', N'322 Bùi Hữu Nghĩa Phường 22 Quận Bình Thạnh', N'0901243787', N'LNV003', N'0')
INSERT [dbo].[NhanVien] ([MaNV], [TenNV], [NgaySinh], [CMND], [DiaChi], [SDT], [LoaiNV], [BiXoa]) VALUES (N'NV011', N'Phạm Hoàng Dung', CAST(0x6C1F0B00 AS Date), N'997867556', N'190 Phạm Văn Đồng Phường 10 Quận Gò Vấp', N'01202322898', N'LNV004', N'0')
INSERT [dbo].[NhanVien] ([MaNV], [TenNV], [NgaySinh], [CMND], [DiaChi], [SDT], [LoaiNV], [BiXoa]) VALUES (N'NV012', N'Phạm Hữu Thạnh', CAST(0x891E0B00 AS Date), N'338898078', N'23 Trần Hưng Đạo Phường 5 Quận 5', N'0969878889', N'LNV002', N'0')
INSERT [dbo].[NhanVien] ([MaNV], [TenNV], [NgaySinh], [CMND], [DiaChi], [SDT], [LoaiNV], [BiXoa]) VALUES (N'NV013', N'Phạm Ninh Xuân', CAST(0xF01E0B00 AS Date), N'234434787', N'222 Phan Văn Trị Phường 10 Quận Gò Vấp', N'01232667545', N'LNV003', N'0')
INSERT [dbo].[NhanVien] ([MaNV], [TenNV], [NgaySinh], [CMND], [DiaChi], [SDT], [LoaiNV], [BiXoa]) VALUES (N'NV014', N'Phạm Ngọc Mai', CAST(0x671E0B00 AS Date), N'349856557', N'9 Nguyễn Văn Cừ Phường 1 Quận 5', N'0987687667', N'LNV004', N'0')
INSERT [dbo].[NhanVien] ([MaNV], [TenNV], [NgaySinh], [CMND], [DiaChi], [SDT], [LoaiNV], [BiXoa]) VALUES (N'NV015', N'Phạm Ngọc Tuyết', CAST(0xCE1E0B00 AS Date), N'343322656', N'78 Lê Văn Việt Phường 3 Quận 9', N'0128898667', N'LNV002', N'0')
INSERT [dbo].[NhanVien] ([MaNV], [TenNV], [NgaySinh], [CMND], [DiaChi], [SDT], [LoaiNV], [BiXoa]) VALUES (N'NV016', N'Trần Minh Hoàng', CAST(0xA41E0B00 AS Date), N'228787656', N'90 Điện Biên Phủ Phường 8 Quận Bình Thạnh', N'0909912178', N'LNV003', N'0')
INSERT [dbo].[NhanVien] ([MaNV], [TenNV], [NgaySinh], [CMND], [DiaChi], [SDT], [LoaiNV], [BiXoa]) VALUES (N'NV017', N'Trần Minh Thiện', CAST(0x651D0B00 AS Date), N'783454222', N'100 Võ Văn Tần Phường 4 Quận 3', N'01229886667', N'LNV004', N'0')
INSERT [dbo].[NhanVien] ([MaNV], [TenNV], [NgaySinh], [CMND], [DiaChi], [SDT], [LoaiNV], [BiXoa]) VALUES (N'NV018', N'Trần Thiện Nhân', CAST(0xE71F0B00 AS Date), N'312276788', N'221 Lý Thường Kiệt Phường 17 Quận 10', N'0985567667', N'LNV002', N'0')
INSERT [dbo].[NhanVien] ([MaNV], [TenNV], [NgaySinh], [CMND], [DiaChi], [SDT], [LoaiNV], [BiXoa]) VALUES (N'NV019', N'Võ Minh Huyền', CAST(0x89210B00 AS Date), N'324434778', N'12 Hoàng Hoa Thám Phường 11 Quận 1', N'01237674645', N'LNV003', N'0')
INSERT [dbo].[NhanVien] ([MaNV], [TenNV], [NgaySinh], [CMND], [DiaChi], [SDT], [LoaiNV], [BiXoa]) VALUES (N'NV020', N'Võ Văn Trung', CAST(0x74230B00 AS Date), N'312245679', N'111 Cao Thắng Phường 11 Quận 3', N'0968988445', N'LNV004', N'0')
INSERT [dbo].[NhanVien] ([MaNV], [TenNV], [NgaySinh], [CMND], [DiaChi], [SDT], [LoaiNV], [BiXoa]) VALUES (N'NV021', N'Nguyễn Trung Trực', CAST(0x68180B00 AS Date), N'312290911', N'Quận 1 HCM', N'0998912190', N'LNV001', N'0')
INSERT [dbo].[NhanVien] ([MaNV], [TenNV], [NgaySinh], [CMND], [DiaChi], [SDT], [LoaiNV], [BiXoa]) VALUES (N'NV022', N'Nguyễn Thị Anh Thư', CAST(0xFA160B00 AS Date), N'310011911', N'Quận 10', N'090122490', N'LNV003', N'0')
INSERT [dbo].[NhanVien] ([MaNV], [TenNV], [NgaySinh], [CMND], [DiaChi], [SDT], [LoaiNV], [BiXoa]) VALUES (N'test', N'test', CAST(0xE63C0B00 AS Date), N'11111', N'11111', N'45454545', N'LNV004', N'1')
/****** Object:  Table [dbo].[LichTrinh]    Script Date: 06/25/2017 11:42:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LichTrinh](
	[MaTuyen] [varchar](10) NOT NULL,
	[NgayDi] [date] NOT NULL,
	[NgayDen] [date] NOT NULL,
	[SoXe] [varchar](15) NOT NULL,
 CONSTRAINT [PK_LichTrinh] PRIMARY KEY CLUSTERED 
(
	[MaTuyen] ASC,
	[NgayDi] ASC,
	[NgayDen] ASC,
	[SoXe] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[LichTrinh] ([MaTuyen], [NgayDi], [NgayDen], [SoXe]) VALUES (N'T001', CAST(0x5F3B0B00 AS Date), CAST(0x5F3B0B00 AS Date), N'10B4-32435')
INSERT [dbo].[LichTrinh] ([MaTuyen], [NgayDi], [NgayDen], [SoXe]) VALUES (N'T001', CAST(0x603B0B00 AS Date), CAST(0x603B0B00 AS Date), N'12B3-00089')
INSERT [dbo].[LichTrinh] ([MaTuyen], [NgayDi], [NgayDen], [SoXe]) VALUES (N'T002', CAST(0x5F3B0B00 AS Date), CAST(0x5F3B0B00 AS Date), N'20B7-43211')
INSERT [dbo].[LichTrinh] ([MaTuyen], [NgayDi], [NgayDen], [SoXe]) VALUES (N'T003', CAST(0x623B0B00 AS Date), CAST(0x623B0B00 AS Date), N'23B1-11553')
INSERT [dbo].[LichTrinh] ([MaTuyen], [NgayDi], [NgayDen], [SoXe]) VALUES (N'T004', CAST(0x633B0B00 AS Date), CAST(0x633B0B00 AS Date), N'25B4-47832')
INSERT [dbo].[LichTrinh] ([MaTuyen], [NgayDi], [NgayDen], [SoXe]) VALUES (N'T005', CAST(0x643B0B00 AS Date), CAST(0x643B0B00 AS Date), N'34B5-00127')
INSERT [dbo].[LichTrinh] ([MaTuyen], [NgayDi], [NgayDen], [SoXe]) VALUES (N'T006', CAST(0x643B0B00 AS Date), CAST(0x643B0B00 AS Date), N'56B2-89110')
INSERT [dbo].[LichTrinh] ([MaTuyen], [NgayDi], [NgayDen], [SoXe]) VALUES (N'T007', CAST(0x653B0B00 AS Date), CAST(0x653B0B00 AS Date), N'60B1-01582')
INSERT [dbo].[LichTrinh] ([MaTuyen], [NgayDi], [NgayDen], [SoXe]) VALUES (N'T008', CAST(0x663B0B00 AS Date), CAST(0x663B0B00 AS Date), N'62B2-87987')
INSERT [dbo].[LichTrinh] ([MaTuyen], [NgayDi], [NgayDen], [SoXe]) VALUES (N'T009', CAST(0x673B0B00 AS Date), CAST(0x673B0B00 AS Date), N'63B1-00001')
INSERT [dbo].[LichTrinh] ([MaTuyen], [NgayDi], [NgayDen], [SoXe]) VALUES (N'T010', CAST(0x683B0B00 AS Date), CAST(0x683B0B00 AS Date), N'65B3-23874')
INSERT [dbo].[LichTrinh] ([MaTuyen], [NgayDi], [NgayDen], [SoXe]) VALUES (N'T011', CAST(0x683B0B00 AS Date), CAST(0x683B0B00 AS Date), N'71B6-10034')
INSERT [dbo].[LichTrinh] ([MaTuyen], [NgayDi], [NgayDen], [SoXe]) VALUES (N'T012', CAST(0x693B0B00 AS Date), CAST(0x693B0B00 AS Date), N'74B3-99331')
INSERT [dbo].[LichTrinh] ([MaTuyen], [NgayDi], [NgayDen], [SoXe]) VALUES (N'T013', CAST(0x6A3B0B00 AS Date), CAST(0x6A3B0B00 AS Date), N'78B3-22115')
INSERT [dbo].[LichTrinh] ([MaTuyen], [NgayDi], [NgayDen], [SoXe]) VALUES (N'T014', CAST(0x6B3B0B00 AS Date), CAST(0x6B3B0B00 AS Date), N'85B1-00003')
INSERT [dbo].[LichTrinh] ([MaTuyen], [NgayDi], [NgayDen], [SoXe]) VALUES (N'T015', CAST(0x6C3B0B00 AS Date), CAST(0x6C3B0B00 AS Date), N'86B2-00011')
INSERT [dbo].[LichTrinh] ([MaTuyen], [NgayDi], [NgayDen], [SoXe]) VALUES (N'T016', CAST(0x6D3B0B00 AS Date), CAST(0x6D3B0B00 AS Date), N'88B7-11703')
INSERT [dbo].[LichTrinh] ([MaTuyen], [NgayDi], [NgayDen], [SoXe]) VALUES (N'T017', CAST(0x6E3B0B00 AS Date), CAST(0x6F3B0B00 AS Date), N'89B2-77862')
INSERT [dbo].[LichTrinh] ([MaTuyen], [NgayDi], [NgayDen], [SoXe]) VALUES (N'T018', CAST(0x6F3B0B00 AS Date), CAST(0x653B0B00 AS Date), N'89B5-67874')
INSERT [dbo].[LichTrinh] ([MaTuyen], [NgayDi], [NgayDen], [SoXe]) VALUES (N'T019', CAST(0x703B0B00 AS Date), CAST(0x703B0B00 AS Date), N'90B1-44351')
INSERT [dbo].[LichTrinh] ([MaTuyen], [NgayDi], [NgayDen], [SoXe]) VALUES (N'T020', CAST(0x713B0B00 AS Date), CAST(0x713B0B00 AS Date), N'90B1-44351')
/****** Object:  Table [dbo].[KhachHang]    Script Date: 06/25/2017 11:42:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[KhachHang](
	[MaKH] [varchar](10) NOT NULL,
	[TenKH] [nvarchar](50) NULL,
	[CMND] [varchar](15) NULL,
	[DiaChi] [nvarchar](50) NULL,
	[SDT] [varchar](15) NULL,
	[HinhThuc] [varchar](10) NULL,
 CONSTRAINT [PK_KhachHang] PRIMARY KEY CLUSTERED 
(
	[MaKH] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [HinhThuc]) VALUES (N'KH001', N'Trần Tầm', N'264412313', N'12 Nguyễn Chí Thanh, Phường 10, Quận 11', N'09122344620', N'H01')
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [HinhThuc]) VALUES (N'KH002', N'Đường Thần Duệ', N'264425461', N'Khánh Hội, Ninh Hải, Ninh Thuận', N'01684151823', N'H01')
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [HinhThuc]) VALUES (N'KH003', N' Tuỳ Vũ Nhi An', N'264123256', N'AH17, Kim Dinh, Tp. Bà Rịa, Bà Rịa - Vũng Tàu', N'01664526524', N'H03')
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [HinhThuc]) VALUES (N'KH004', N'Tịch Si Thần', N'264597853', N' 7/7 Hoàng Đạo Thành, Thanh Xuân, Hà Nội', N'09621543225', N'H02')
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [HinhThuc]) VALUES (N'KH005', N'Nguyễn Trung Linh', N'321563590', N' QL21, Tân Lập, Hàm Thuận Nam, Bình Thuận', N'01235486221', N'H01')
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [HinhThuc]) VALUES (N'KH006', N'Lâm Dương', N'125633256', N'32 Nguyễn Tất Thành, Thị xã Hương Thủy, Huế', N'01652458633', N'H02')
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [HinhThuc]) VALUES (N'KH007', N'Quách Kính Minh', N'245896324', N'25/12 Trần Hưng Đạo, tp.Hội An, Đà Nẵng', N'01664751283', N'H03')
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [HinhThuc]) VALUES (N'KH008', N'Dư Mặc', N'524632893', N'44 Nguyễn Bỉnh Khiêm, Xương Huân, Tp. Nha Trang', N'01689532456', N'H01')
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [HinhThuc]) VALUES (N'KH009', N'Phương Dư Khả', N'345682632', N'67 Nguyễn Trãi
Phường 9,
Tp. Đà Lạt, 
Lâm Đồng', N'09235468965', N'H01')
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [HinhThuc]) VALUES (N'KH010', N'Lâm Khải Chính', N'246523586', N'534 Thiên Lôi, 
Vĩnh Niệm
, Lê Chân, 
Hải Phòng', N'09625422356', N'H01')
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [HinhThuc]) VALUES (N'KH011', N'Hàn Tiềm', N'542632563', N'125/2 Đinh Bộ Lĩnh, Phường 13, Quận Bình Thạnh', N'01695232457', N'H02')
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [HinhThuc]) VALUES (N'KH012', N'La Đông Phong ', N'265896325', N'Tri Thủy, Ninh Hải, Ninh Thuận', N'09625468310', N'H03')
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [HinhThuc]) VALUES (N'KH013', N'Hoắc Triển Bạch', N'356847962', N'51 Phạm Văn Đồng, TP.Nha Trang, Khánh Hòa', N'01658469321', N'H01')
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [HinhThuc]) VALUES (N'KH014', N'Mạc Thiệu Khiêm', N'256489353', N'115 Nguyễn Đình Chiểu, Phường 12, Quận 3, Tp.HCM', N'09615236548', N'H02')
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [HinhThuc]) VALUES (N'KH015', N'Lý Thừa Ngân', N'652345220', N'65 Quang Trung, Phường 5, Gò Vấp, Tp.HCM', N'01684523659', N'H01')
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [HinhThuc]) VALUES (N'KH016', N'Dạ Thiên Lăng', N'24569856', N'18 Nguyễn Văn Cừ, phường 3, Quận 5, Tp.HCM', N'09125489652', N'H01')
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [HinhThuc]) VALUES (N'KH017', N'Tề Mặc', N'54589623', N'264 Huỳnh Văn Cù, Tp.Thủ Dầu Một, Bình Dương', N'01685459630', N'H01')
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [HinhThuc]) VALUES (N'KH018', N'Vũ Văn Duệ', N'24596852', N'256 Cách Mạng Tháng 8, Phường 5, Quận 3, Tp.HCM', N'09652352632', N'H02')
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [HinhThuc]) VALUES (N'KH019', N'Lý Mộc Ngư', N'65869631', N'26 Điện Biên Phủ, P.12, Quận Bình Thạnh, Tp.HCM', N'01685326995', N'H01')
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [HinhThuc]) VALUES (N'KH020', N'Tần Mạc ', N'25489655', N'78 Võ Văn Tần, Phường 3,Quận 3, Tp.HCM', N'09124856963', N'H03')
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [HinhThuc]) VALUES (N'KH21', N'Nguyễn Thị Khánh', N'312285036', N'quận 2', N'01644460699', NULL)
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [HinhThuc]) VALUES (N'KH22', N'Nguyễn Minh Nhân', N'312285858', N'Quận Bình Tân', N'01265926255', NULL)
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [HinhThuc]) VALUES (N'KH23', N'Nguyễn Thị Khánh', N'312285036', N'Quận 1', N'01644460699', NULL)
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [HinhThuc]) VALUES (N'KH24', N'Nguyễn Thị B', N'31222222', N'Quận 8', N'019898988', NULL)
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [HinhThuc]) VALUES (N'KH25', N'Nguyễn Văn Lực', N'312280099', N'223 Bạch Đằng Quận Bình Thạnh', N'0129112233', NULL)
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [HinhThuc]) VALUES (N'KH26', N'Trần Minh Thái', N'210990876', N'14/1 D2 TPHCM', N'0901212876', NULL)
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [HinhThuc]) VALUES (N'KH27', N'Nguyễn Thị Ngọc', N'2189888227', N'Quận 1', N'090121131212', NULL)
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [HinhThuc]) VALUES (N'KH28', N'Nguyễn Thị Minh', N'2189888223', N'Quận 1', N'0901211345', NULL)
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [HinhThuc]) VALUES (N'KH29', N'Nguyễn Văn D', N'333110000', N'Quận 10', N'0901122390', NULL)
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [HinhThuc]) VALUES (N'KH30', N'Nguyễn Thi E', N'2222110911', N'Quận 9', N'09198981223', NULL)
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [HinhThuc]) VALUES (N'KH31', N'Nguyễn Thi C', N'222211090', N'Quận 10', N'0919898123', NULL)
/****** Object:  Table [dbo].[VeXe]    Script Date: 06/25/2017 11:42:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[VeXe](
	[MaVe] [varchar](10) NOT NULL,
	[MaGhe] [varchar](10) NULL,
	[MaKH] [varchar](10) NULL,
	[NgayDi] [date] NULL,
	[GioDi] [time](7) NULL,
	[SoXe] [varchar](15) NULL,
	[GiaVe] [real] NULL,
	[NhanVienBV] [varchar](10) NULL,
 CONSTRAINT [PK_VeXe] PRIMARY KEY CLUSTERED 
(
	[MaVe] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[VeXe] ([MaVe], [MaGhe], [MaKH], [NgayDi], [GioDi], [SoXe], [GiaVe], [NhanVienBV]) VALUES (N'V001', N'GH002', N'KH005', CAST(0xAE3C0B00 AS Date), CAST(0x0700E03495640000 AS Time), N'12B3-00089', 50000, N'NV001')
INSERT [dbo].[VeXe] ([MaVe], [MaGhe], [MaKH], [NgayDi], [GioDi], [SoXe], [GiaVe], [NhanVienBV]) VALUES (N'V002', N'GH003', N'KH009', CAST(0x863C0B00 AS Date), CAST(0x07007870335C0000 AS Time), N'20B7-43211', 100000, N'NV002')
INSERT [dbo].[VeXe] ([MaVe], [MaGhe], [MaKH], [NgayDi], [GioDi], [SoXe], [GiaVe], [NhanVienBV]) VALUES (N'V003', N'GH004', N'KH003', CAST(0xA63C0B00 AS Date), CAST(0x0700D088C3100000 AS Time), N'23B1-11553', 200000, N'NV003')
INSERT [dbo].[VeXe] ([MaVe], [MaGhe], [MaKH], [NgayDi], [GioDi], [SoXe], [GiaVe], [NhanVienBV]) VALUES (N'V004', N'GH005', N'KH001', CAST(0xA73C0B00 AS Date), CAST(0x0700A8E76F4B0000 AS Time), N'10B4-32435', 150000, N'NV004')
INSERT [dbo].[VeXe] ([MaVe], [MaGhe], [MaKH], [NgayDi], [GioDi], [SoXe], [GiaVe], [NhanVienBV]) VALUES (N'V005', N'GH006', N'KH004', CAST(0xE63C0B00 AS Date), CAST(0x070010ACD1530000 AS Time), N'25B4-47832', 300000, N'NV005')
INSERT [dbo].[VeXe] ([MaVe], [MaGhe], [MaKH], [NgayDi], [GioDi], [SoXe], [GiaVe], [NhanVienBV]) VALUES (N'V006', N'GH007', N'KH002', CAST(0xA53C0B00 AS Date), CAST(0x0700F0E066B80000 AS Time), N'34B5-00127', 200000, N'NV006')
INSERT [dbo].[VeXe] ([MaVe], [MaGhe], [MaKH], [NgayDi], [GioDi], [SoXe], [GiaVe], [NhanVienBV]) VALUES (N'V007', N'GH010', N'KH007', CAST(0xC63C0B00 AS Date), CAST(0x0700384D25190000 AS Time), N'56B2-89110', 100000, N'NV007')
INSERT [dbo].[VeXe] ([MaVe], [MaGhe], [MaKH], [NgayDi], [GioDi], [SoXe], [GiaVe], [NhanVienBV]) VALUES (N'V008', N'GH020', N'KH008', CAST(0x733C0B00 AS Date), CAST(0x070040230E430000 AS Time), N'60B1-01582', 300000, N'NV008')
INSERT [dbo].[VeXe] ([MaVe], [MaGhe], [MaKH], [NgayDi], [GioDi], [SoXe], [GiaVe], [NhanVienBV]) VALUES (N'V009', N'GH030', N'KH006', CAST(0x6A3C0B00 AS Date), CAST(0x070008D6E8290000 AS Time), N'62B2-87987', 500000, N'NV009')
INSERT [dbo].[VeXe] ([MaVe], [MaGhe], [MaKH], [NgayDi], [GioDi], [SoXe], [GiaVe], [NhanVienBV]) VALUES (N'V010', N'GH040', N'KH011', CAST(0x873C0B00 AS Date), CAST(0x07002058A3A70000 AS Time), N'63B1-00001', 400000, N'NV010')
INSERT [dbo].[VeXe] ([MaVe], [MaGhe], [MaKH], [NgayDi], [GioDi], [SoXe], [GiaVe], [NhanVienBV]) VALUES (N'V011', N'GH050', N'KH010', CAST(0x213D0B00 AS Date), CAST(0x070050CFDF960000 AS Time), N'65B3-23874', 200000, N'NV011')
INSERT [dbo].[VeXe] ([MaVe], [MaGhe], [MaKH], [NgayDi], [GioDi], [SoXe], [GiaVe], [NhanVienBV]) VALUES (N'V012', N'GH060', N'KH013', CAST(0x253D0B00 AS Date), CAST(0x070048F9F66C0000 AS Time), N'71B6-10034', 120000, N'NV012')
INSERT [dbo].[VeXe] ([MaVe], [MaGhe], [MaKH], [NgayDi], [GioDi], [SoXe], [GiaVe], [NhanVienBV]) VALUES (N'V013', N'GH070', N'KH012', CAST(0xA73C0B00 AS Date), CAST(0x0700D85EAC3A0000 AS Time), N'74B3-99331', 150000, N'NV013')
INSERT [dbo].[VeXe] ([MaVe], [MaGhe], [MaKH], [NgayDi], [GioDi], [SoXe], [GiaVe], [NhanVienBV]) VALUES (N'V014', N'GH100', N'KH015', CAST(0x9C3D0B00 AS Date), CAST(0x070068C461080000 AS Time), N'78B3-22115', 400000, N'NV014')
INSERT [dbo].[VeXe] ([MaVe], [MaGhe], [MaKH], [NgayDi], [GioDi], [SoXe], [GiaVe], [NhanVienBV]) VALUES (N'V015', N'GH200', N'KH014', CAST(0x7D3D0B00 AS Date), CAST(0x070080461C860000 AS Time), N'85B1-00003', 100000, N'NV015')
INSERT [dbo].[VeXe] ([MaVe], [MaGhe], [MaKH], [NgayDi], [GioDi], [SoXe], [GiaVe], [NhanVienBV]) VALUES (N'V016', N'GH300', N'KH020', CAST(0x033D0B00 AS Date), CAST(0x070008D6E8290000 AS Time), N'86B2-00011', 200000, N'NV016')
INSERT [dbo].[VeXe] ([MaVe], [MaGhe], [MaKH], [NgayDi], [GioDi], [SoXe], [GiaVe], [NhanVienBV]) VALUES (N'V017', N'GH400', N'KH017', CAST(0xA73C0B00 AS Date), CAST(0x0700709A4A320000 AS Time), N'88B7-11703', 300000, N'NV017')
INSERT [dbo].[VeXe] ([MaVe], [MaGhe], [MaKH], [NgayDi], [GioDi], [SoXe], [GiaVe], [NhanVienBV]) VALUES (N'V018', N'GH500', N'KH016', CAST(0xA83C0B00 AS Date), CAST(0x07002058A3A70000 AS Time), N'89B2-77862', 50000, N'NV018')
INSERT [dbo].[VeXe] ([MaVe], [MaGhe], [MaKH], [NgayDi], [GioDi], [SoXe], [GiaVe], [NhanVienBV]) VALUES (N'V019', N'GH600', N'KH019', CAST(0xA93C0B00 AS Date), CAST(0x0700D85EAC3A0000 AS Time), N'89B5-67874', 400000, N'NV019')
INSERT [dbo].[VeXe] ([MaVe], [MaGhe], [MaKH], [NgayDi], [GioDi], [SoXe], [GiaVe], [NhanVienBV]) VALUES (N'V020', N'GH130', N'KH018', CAST(0xC83C0B00 AS Date), CAST(0x070068C461080000 AS Time), N'90B1-44351', 100000, N'NV020')
INSERT [dbo].[VeXe] ([MaVe], [MaGhe], [MaKH], [NgayDi], [GioDi], [SoXe], [GiaVe], [NhanVienBV]) VALUES (N'V021', N'GH04', N'KH21', CAST(0x623B0B00 AS Date), CAST(0x0700E03495640000 AS Time), N'23B1-11553', 200000, N'NV001')
INSERT [dbo].[VeXe] ([MaVe], [MaGhe], [MaKH], [NgayDi], [GioDi], [SoXe], [GiaVe], [NhanVienBV]) VALUES (N'V022', N'GH04', N'KH24', CAST(0x5F3B0B00 AS Date), CAST(0x0700709A4A320000 AS Time), N'10B4-32435', 400000, N'NV001')
INSERT [dbo].[VeXe] ([MaVe], [MaGhe], [MaKH], [NgayDi], [GioDi], [SoXe], [GiaVe], [NhanVienBV]) VALUES (N'V023', N'GH05', N'KH25', CAST(0x633B0B00 AS Date), CAST(0x07007870335C0000 AS Time), N'25B4-47832', 90000, N'NV001')
INSERT [dbo].[VeXe] ([MaVe], [MaGhe], [MaKH], [NgayDi], [GioDi], [SoXe], [GiaVe], [NhanVienBV]) VALUES (N'V024', N'GH11', N'KH26', CAST(0x603B0B00 AS Date), CAST(0x0700709A4A320000 AS Time), N'12B3-00089', 9000000, N'NV001')
INSERT [dbo].[VeXe] ([MaVe], [MaGhe], [MaKH], [NgayDi], [GioDi], [SoXe], [GiaVe], [NhanVienBV]) VALUES (N'V025', N'GH02', N'KH29', CAST(0x623B0B00 AS Date), CAST(0x0700E03495640000 AS Time), N'23B1-11553', 90000, N'NV001')
/****** Object:  Table [dbo].[HangHoa]    Script Date: 06/25/2017 11:42:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HangHoa](
	[MaHH] [varchar](10) NOT NULL,
	[LoaiHH] [nvarchar](50) NULL,
	[KhoiLuong] [float] NULL,
	[Phi] [real] NULL,
	[NguoiGui] [varchar](10) NULL,
	[NguoiNhan] [varchar](10) NULL,
	[NhanVienNhan] [varchar](10) NULL,
 CONSTRAINT [PK_HangHoa] PRIMARY KEY CLUSTERED 
(
	[MaHH] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[HangHoa] ([MaHH], [LoaiHH], [KhoiLuong], [Phi], [NguoiGui], [NguoiNhan], [NhanVienNhan]) VALUES (N'HH001', N'Máy ảnh', 30, 20000, N'KH001', N'KH005', N'NV001')
INSERT [dbo].[HangHoa] ([MaHH], [LoaiHH], [KhoiLuong], [Phi], [NguoiGui], [NguoiNhan], [NhanVienNhan]) VALUES (N'HH002', N'Xe máy', 40, 20000, N'KH002', N'KH011', N'NV002')
INSERT [dbo].[HangHoa] ([MaHH], [LoaiHH], [KhoiLuong], [Phi], [NguoiGui], [NguoiNhan], [NhanVienNhan]) VALUES (N'HH003', N'Giấy tờ', 1, 10000, N'KH003', N'KH020', N'NV003')
INSERT [dbo].[HangHoa] ([MaHH], [LoaiHH], [KhoiLuong], [Phi], [NguoiGui], [NguoiNhan], [NhanVienNhan]) VALUES (N'HH004', N'Thực phẩm', 5, 20000, N'KH004', N'KH009', N'NV004')
INSERT [dbo].[HangHoa] ([MaHH], [LoaiHH], [KhoiLuong], [Phi], [NguoiGui], [NguoiNhan], [NhanVienNhan]) VALUES (N'HH005', N'Điện thoại', 10, 20000, N'KH005', N'KH002', N'NV005')
INSERT [dbo].[HangHoa] ([MaHH], [LoaiHH], [KhoiLuong], [Phi], [NguoiGui], [NguoiNhan], [NhanVienNhan]) VALUES (N'HH006', N'Tiền mặt', 5, 20000, N'KH006', N'KH001', N'NV006')
INSERT [dbo].[HangHoa] ([MaHH], [LoaiHH], [KhoiLuong], [Phi], [NguoiGui], [NguoiNhan], [NhanVienNhan]) VALUES (N'HH007', N'Giấy tờ', 1, 10000, N'KH007', N'KH012', N'NV007')
INSERT [dbo].[HangHoa] ([MaHH], [LoaiHH], [KhoiLuong], [Phi], [NguoiGui], [NguoiNhan], [NhanVienNhan]) VALUES (N'HH008', N'Thực phẩm', 20, 20000, N'KH008', N'KH019', N'NV008')
INSERT [dbo].[HangHoa] ([MaHH], [LoaiHH], [KhoiLuong], [Phi], [NguoiGui], [NguoiNhan], [NhanVienNhan]) VALUES (N'HH009', N'Xe máy', 40, 20000, N'KH009', N'KH004', N'NV009')
INSERT [dbo].[HangHoa] ([MaHH], [LoaiHH], [KhoiLuong], [Phi], [NguoiGui], [NguoiNhan], [NhanVienNhan]) VALUES (N'HH010', N'Máy ảnh', 10, 20000, N'KH010', N'KH003', N'NV010')
INSERT [dbo].[HangHoa] ([MaHH], [LoaiHH], [KhoiLuong], [Phi], [NguoiGui], [NguoiNhan], [NhanVienNhan]) VALUES (N'HH011', N'Điện thoại', 1, 10000, N'KH011', N'KH002', N'NV011')
INSERT [dbo].[HangHoa] ([MaHH], [LoaiHH], [KhoiLuong], [Phi], [NguoiGui], [NguoiNhan], [NhanVienNhan]) VALUES (N'HH012', N'Điện thoại', 5, 20000, N'KH012', N'KH005', N'NV012')
INSERT [dbo].[HangHoa] ([MaHH], [LoaiHH], [KhoiLuong], [Phi], [NguoiGui], [NguoiNhan], [NhanVienNhan]) VALUES (N'HH013', N'Giấy tờ', 2, 10000, N'KH013', N'KH006', N'NV013')
INSERT [dbo].[HangHoa] ([MaHH], [LoaiHH], [KhoiLuong], [Phi], [NguoiGui], [NguoiNhan], [NhanVienNhan]) VALUES (N'HH014', N'Thực phẩm', 40, 20000, N'KH014', N'KH007', N'NV014')
INSERT [dbo].[HangHoa] ([MaHH], [LoaiHH], [KhoiLuong], [Phi], [NguoiGui], [NguoiNhan], [NhanVienNhan]) VALUES (N'HH015', N'Hàng dễ vỡ', 76, 20000, N'KH015', N'KH018', N'NV015')
INSERT [dbo].[HangHoa] ([MaHH], [LoaiHH], [KhoiLuong], [Phi], [NguoiGui], [NguoiNhan], [NhanVienNhan]) VALUES (N'HH016', N'Giấy tờ', 2, 10000, N'KH016', N'KH010', N'NV016')
INSERT [dbo].[HangHoa] ([MaHH], [LoaiHH], [KhoiLuong], [Phi], [NguoiGui], [NguoiNhan], [NhanVienNhan]) VALUES (N'HH017', N'Xe máy', 20, 20000, N'KH017', N'KH017', N'NV017')
INSERT [dbo].[HangHoa] ([MaHH], [LoaiHH], [KhoiLuong], [Phi], [NguoiGui], [NguoiNhan], [NhanVienNhan]) VALUES (N'HH018', N'Thực phẩm', 30, 20000, N'KH018', N'KH006', N'NV018')
INSERT [dbo].[HangHoa] ([MaHH], [LoaiHH], [KhoiLuong], [Phi], [NguoiGui], [NguoiNhan], [NhanVienNhan]) VALUES (N'HH019', N'Giấy tờ', 2, 10000, N'KH019', N'KH012', N'NV019')
INSERT [dbo].[HangHoa] ([MaHH], [LoaiHH], [KhoiLuong], [Phi], [NguoiGui], [NguoiNhan], [NhanVienNhan]) VALUES (N'HH020', N'Giấy tờ', 3, 10000, N'KH020', N'KH007', N'NV020')
INSERT [dbo].[HangHoa] ([MaHH], [LoaiHH], [KhoiLuong], [Phi], [NguoiGui], [NguoiNhan], [NhanVienNhan]) VALUES (N'HH21', N'Hàng dễ vỡ', 5, 300000, N'KH22', N'KH23', N'NV021')
INSERT [dbo].[HangHoa] ([MaHH], [LoaiHH], [KhoiLuong], [Phi], [NguoiGui], [NguoiNhan], [NhanVienNhan]) VALUES (N'HH22', N'Máy ảnh', 90000, 98000, N'KH27', N'KH28', N'NV022')
INSERT [dbo].[HangHoa] ([MaHH], [LoaiHH], [KhoiLuong], [Phi], [NguoiGui], [NguoiNhan], [NhanVienNhan]) VALUES (N'HH23', N'Tivi', 90, 100000, N'KH30', N'KH31', NULL)
/****** Object:  Table [dbo].[Ghe]    Script Date: 06/25/2017 11:42:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ghe](
	[MaGhe] [varchar](10) NOT NULL,
	[MaTuyen] [varchar](10) NULL,
	[MaKH] [varchar](10) NULL,
	[TinhTrang] [nvarchar](50) NULL,
	[SoXe] [varchar](15) NULL,
 CONSTRAINT [PK_Ghe] PRIMARY KEY CLUSTERED 
(
	[MaGhe] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[Ghe] ([MaGhe], [MaTuyen], [MaKH], [TinhTrang], [SoXe]) VALUES (N'GH001', N'T001', N'KH001', N'Đã đặt', N'10B4-32435')
INSERT [dbo].[Ghe] ([MaGhe], [MaTuyen], [MaKH], [TinhTrang], [SoXe]) VALUES (N'GH002', N'T001', N'KH002', N'Đã đặt', N'10B4-32435')
INSERT [dbo].[Ghe] ([MaGhe], [MaTuyen], [MaKH], [TinhTrang], [SoXe]) VALUES (N'GH003', N'T001', NULL, N'Trống', N'10B4-32435    ')
INSERT [dbo].[Ghe] ([MaGhe], [MaTuyen], [MaKH], [TinhTrang], [SoXe]) VALUES (N'GH004', N'T001', N'KH005', N'Đã đặt', N'10B4-32435')
INSERT [dbo].[Ghe] ([MaGhe], [MaTuyen], [MaKH], [TinhTrang], [SoXe]) VALUES (N'GH005', N'T001', N'KH005', N'Đã đặt', N'10B4-32435')
INSERT [dbo].[Ghe] ([MaGhe], [MaTuyen], [MaKH], [TinhTrang], [SoXe]) VALUES (N'GH006', N'T001', N'KH008', N'Đã đặt', N'10B4-32435')
INSERT [dbo].[Ghe] ([MaGhe], [MaTuyen], [MaKH], [TinhTrang], [SoXe]) VALUES (N'GH007', N'T001', N'KH008', N'Đã đặt', N'10B4-32435')
INSERT [dbo].[Ghe] ([MaGhe], [MaTuyen], [MaKH], [TinhTrang], [SoXe]) VALUES (N'GH010', N'T002', N'KH009', N'Đã đặt', N'25B4-47832')
INSERT [dbo].[Ghe] ([MaGhe], [MaTuyen], [MaKH], [TinhTrang], [SoXe]) VALUES (N'GH020', N'T002', N'KH009', N'Đã đặt', N'25B4-47832')
INSERT [dbo].[Ghe] ([MaGhe], [MaTuyen], [MaKH], [TinhTrang], [SoXe]) VALUES (N'GH030', N'T002', N'KH009', N'Đã đặt', N'25B4-47832')
INSERT [dbo].[Ghe] ([MaGhe], [MaTuyen], [MaKH], [TinhTrang], [SoXe]) VALUES (N'GH040', N'T002', NULL, N'Trống', N'25B4-47832')
INSERT [dbo].[Ghe] ([MaGhe], [MaTuyen], [MaKH], [TinhTrang], [SoXe]) VALUES (N'GH050', N'T002', NULL, N'Trống', N'25B4-47832')
INSERT [dbo].[Ghe] ([MaGhe], [MaTuyen], [MaKH], [TinhTrang], [SoXe]) VALUES (N'GH060', N'T002', NULL, N'Trống', N'25B4-47832')
INSERT [dbo].[Ghe] ([MaGhe], [MaTuyen], [MaKH], [TinhTrang], [SoXe]) VALUES (N'GH070', N'T002', N'KH010', N'Đã đặt', N'25B4-47832')
INSERT [dbo].[Ghe] ([MaGhe], [MaTuyen], [MaKH], [TinhTrang], [SoXe]) VALUES (N'GH100', N'T004', N'KH013', N'Đã đặt', N'56B2-89110')
INSERT [dbo].[Ghe] ([MaGhe], [MaTuyen], [MaKH], [TinhTrang], [SoXe]) VALUES (N'GH110', N'T003', NULL, N'Trống', N'78B3-22115')
INSERT [dbo].[Ghe] ([MaGhe], [MaTuyen], [MaKH], [TinhTrang], [SoXe]) VALUES (N'GH120', N'T003', NULL, N'Trống', N'78B3-22115')
INSERT [dbo].[Ghe] ([MaGhe], [MaTuyen], [MaKH], [TinhTrang], [SoXe]) VALUES (N'GH130', N'T003', NULL, N'Trống', N'78B3-22115')
INSERT [dbo].[Ghe] ([MaGhe], [MaTuyen], [MaKH], [TinhTrang], [SoXe]) VALUES (N'GH140', N'T003', N'KH019', N'Đã đặt', N'78B3-22115')
INSERT [dbo].[Ghe] ([MaGhe], [MaTuyen], [MaKH], [TinhTrang], [SoXe]) VALUES (N'GH200', N'T004', N'KH015', N'Đã đặt', N'56B2-89110')
INSERT [dbo].[Ghe] ([MaGhe], [MaTuyen], [MaKH], [TinhTrang], [SoXe]) VALUES (N'GH300', N'T004', N'KH015', N'Đã đặt', N'56B2-89110')
INSERT [dbo].[Ghe] ([MaGhe], [MaTuyen], [MaKH], [TinhTrang], [SoXe]) VALUES (N'GH400', N'T004', N'KH016', N'Đã đặt', N'56B2-89110')
INSERT [dbo].[Ghe] ([MaGhe], [MaTuyen], [MaKH], [TinhTrang], [SoXe]) VALUES (N'GH500', N'T004', N'KH016', N'Đã đặt', N'56B2-89110')
INSERT [dbo].[Ghe] ([MaGhe], [MaTuyen], [MaKH], [TinhTrang], [SoXe]) VALUES (N'GH600', N'T004', N'KH013', N'Đã đặt', N'56B2-89110')
INSERT [dbo].[Ghe] ([MaGhe], [MaTuyen], [MaKH], [TinhTrang], [SoXe]) VALUES (N'GH700', N'T004', NULL, N'Trống', N'56B2-89110')
/****** Object:  Table [dbo].[HoaDonVe]    Script Date: 06/25/2017 11:42:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HoaDonVe](
	[MaHD] [varchar](10) NULL,
	[MaVe] [varchar](10) NULL,
	[MaKH] [varchar](10) NULL,
	[SoLuong] [int] NULL,
	[TongTien] [real] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[HoaDonVe] ([MaHD], [MaVe], [MaKH], [SoLuong], [TongTien]) VALUES (N'HV001', N'V020', N'KH018', 0, 0)
INSERT [dbo].[HoaDonVe] ([MaHD], [MaVe], [MaKH], [SoLuong], [TongTien]) VALUES (N'HV002', N'V019', N'KH019', 1, 400000)
INSERT [dbo].[HoaDonVe] ([MaHD], [MaVe], [MaKH], [SoLuong], [TongTien]) VALUES (N'HV003', N'V018', N'KH016', 2, 100000)
INSERT [dbo].[HoaDonVe] ([MaHD], [MaVe], [MaKH], [SoLuong], [TongTien]) VALUES (N'HV004', N'V017', N'KH017', 0, 0)
INSERT [dbo].[HoaDonVe] ([MaHD], [MaVe], [MaKH], [SoLuong], [TongTien]) VALUES (N'HV005', N'V016', N'KH020', 0, 0)
INSERT [dbo].[HoaDonVe] ([MaHD], [MaVe], [MaKH], [SoLuong], [TongTien]) VALUES (N'HV006', N'V015', N'KH014', 0, 0)
INSERT [dbo].[HoaDonVe] ([MaHD], [MaVe], [MaKH], [SoLuong], [TongTien]) VALUES (N'HV007', N'V014', N'KH015', 2, 800000)
INSERT [dbo].[HoaDonVe] ([MaHD], [MaVe], [MaKH], [SoLuong], [TongTien]) VALUES (N'HV008', N'V013', N'KH012', 0, 0)
INSERT [dbo].[HoaDonVe] ([MaHD], [MaVe], [MaKH], [SoLuong], [TongTien]) VALUES (N'HV009', N'V012', N'KH013     ', 2, 240000)
INSERT [dbo].[HoaDonVe] ([MaHD], [MaVe], [MaKH], [SoLuong], [TongTien]) VALUES (N'HV010', N'V011', N'KH010', 1, 200000)
INSERT [dbo].[HoaDonVe] ([MaHD], [MaVe], [MaKH], [SoLuong], [TongTien]) VALUES (N'HV011', N'V010', N'KH011', 0, 0)
INSERT [dbo].[HoaDonVe] ([MaHD], [MaVe], [MaKH], [SoLuong], [TongTien]) VALUES (N'HV012', N'V009', N'KH006', 0, 0)
INSERT [dbo].[HoaDonVe] ([MaHD], [MaVe], [MaKH], [SoLuong], [TongTien]) VALUES (N'HV013', N'V008', N'KH008', 2, 600000)
INSERT [dbo].[HoaDonVe] ([MaHD], [MaVe], [MaKH], [SoLuong], [TongTien]) VALUES (N'HV014', N'V007', N'KH007', 0, 0)
INSERT [dbo].[HoaDonVe] ([MaHD], [MaVe], [MaKH], [SoLuong], [TongTien]) VALUES (N'HV015', N'V006', N'KH002', 1, 200000)
INSERT [dbo].[HoaDonVe] ([MaHD], [MaVe], [MaKH], [SoLuong], [TongTien]) VALUES (N'HV016', N'V005', N'KH004', 0, 0)
INSERT [dbo].[HoaDonVe] ([MaHD], [MaVe], [MaKH], [SoLuong], [TongTien]) VALUES (N'HV017', N'V004', N'KH001', 1, 150000)
INSERT [dbo].[HoaDonVe] ([MaHD], [MaVe], [MaKH], [SoLuong], [TongTien]) VALUES (N'HV018', N'V003', N'KH003', 0, 0)
INSERT [dbo].[HoaDonVe] ([MaHD], [MaVe], [MaKH], [SoLuong], [TongTien]) VALUES (N'HV019', N'V002', N'KH009', 3, 300000)
INSERT [dbo].[HoaDonVe] ([MaHD], [MaVe], [MaKH], [SoLuong], [TongTien]) VALUES (N'HV020', N'V001', N'KH005', 2, 100000)
/****** Object:  Table [dbo].[HoaDonHH]    Script Date: 06/25/2017 11:42:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HoaDonHH](
	[MaHD] [varchar](10) NOT NULL,
	[MaHH] [varchar](10) NULL,
	[MaKH] [varchar](10) NULL,
	[TongTien] [real] NULL,
 CONSTRAINT [PK_HoaDonHH] PRIMARY KEY CLUSTERED 
(
	[MaHD] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[HoaDonHH] ([MaHD], [MaHH], [MaKH], [TongTien]) VALUES (N'HD004', N'HH004', N'KH004', 100000)
INSERT [dbo].[HoaDonHH] ([MaHD], [MaHH], [MaKH], [TongTien]) VALUES (N'HD006', N'HH006', N'KH006', 100000)
INSERT [dbo].[HoaDonHH] ([MaHD], [MaHH], [MaKH], [TongTien]) VALUES (N'HD011', N'HH011', N'KH011', 10000)
INSERT [dbo].[HoaDonHH] ([MaHD], [MaHH], [MaKH], [TongTien]) VALUES (N'HD014', N'HH014', N'KH014', 80000)
INSERT [dbo].[HoaDonHH] ([MaHD], [MaHH], [MaKH], [TongTien]) VALUES (N'HD018', N'HH018', N'KH018', 600000)
/****** Object:  ForeignKey [FK_Ghe_KhachHang]    Script Date: 06/25/2017 11:42:13 ******/
ALTER TABLE [dbo].[Ghe]  WITH CHECK ADD  CONSTRAINT [FK_Ghe_KhachHang] FOREIGN KEY([MaKH])
REFERENCES [dbo].[KhachHang] ([MaKH])
GO
ALTER TABLE [dbo].[Ghe] CHECK CONSTRAINT [FK_Ghe_KhachHang]
GO
/****** Object:  ForeignKey [FK_Ghe_TuyenXe]    Script Date: 06/25/2017 11:42:13 ******/
ALTER TABLE [dbo].[Ghe]  WITH CHECK ADD  CONSTRAINT [FK_Ghe_TuyenXe] FOREIGN KEY([MaTuyen])
REFERENCES [dbo].[TuyenXe] ([MaTuyen])
GO
ALTER TABLE [dbo].[Ghe] CHECK CONSTRAINT [FK_Ghe_TuyenXe]
GO
/****** Object:  ForeignKey [FK_HangHoa_NhanVien]    Script Date: 06/25/2017 11:42:13 ******/
ALTER TABLE [dbo].[HangHoa]  WITH CHECK ADD  CONSTRAINT [FK_HangHoa_NhanVien] FOREIGN KEY([NhanVienNhan])
REFERENCES [dbo].[NhanVien] ([MaNV])
GO
ALTER TABLE [dbo].[HangHoa] CHECK CONSTRAINT [FK_HangHoa_NhanVien]
GO
/****** Object:  ForeignKey [FK_HoaDonHH_HangHoa]    Script Date: 06/25/2017 11:42:13 ******/
ALTER TABLE [dbo].[HoaDonHH]  WITH CHECK ADD  CONSTRAINT [FK_HoaDonHH_HangHoa] FOREIGN KEY([MaHH])
REFERENCES [dbo].[HangHoa] ([MaHH])
GO
ALTER TABLE [dbo].[HoaDonHH] CHECK CONSTRAINT [FK_HoaDonHH_HangHoa]
GO
/****** Object:  ForeignKey [FK_HoaDonHH_KhachHang]    Script Date: 06/25/2017 11:42:13 ******/
ALTER TABLE [dbo].[HoaDonHH]  WITH CHECK ADD  CONSTRAINT [FK_HoaDonHH_KhachHang] FOREIGN KEY([MaKH])
REFERENCES [dbo].[KhachHang] ([MaKH])
GO
ALTER TABLE [dbo].[HoaDonHH] CHECK CONSTRAINT [FK_HoaDonHH_KhachHang]
GO
/****** Object:  ForeignKey [FK_HoaDonVe_KhachHang]    Script Date: 06/25/2017 11:42:13 ******/
ALTER TABLE [dbo].[HoaDonVe]  WITH CHECK ADD  CONSTRAINT [FK_HoaDonVe_KhachHang] FOREIGN KEY([MaKH])
REFERENCES [dbo].[KhachHang] ([MaKH])
GO
ALTER TABLE [dbo].[HoaDonVe] CHECK CONSTRAINT [FK_HoaDonVe_KhachHang]
GO
/****** Object:  ForeignKey [FK_HoaDonVe_VeXe]    Script Date: 06/25/2017 11:42:13 ******/
ALTER TABLE [dbo].[HoaDonVe]  WITH CHECK ADD  CONSTRAINT [FK_HoaDonVe_VeXe] FOREIGN KEY([MaVe])
REFERENCES [dbo].[VeXe] ([MaVe])
GO
ALTER TABLE [dbo].[HoaDonVe] CHECK CONSTRAINT [FK_HoaDonVe_VeXe]
GO
/****** Object:  ForeignKey [FK_KhachHang_HinhThuc]    Script Date: 06/25/2017 11:42:13 ******/
ALTER TABLE [dbo].[KhachHang]  WITH CHECK ADD  CONSTRAINT [FK_KhachHang_HinhThuc] FOREIGN KEY([HinhThuc])
REFERENCES [dbo].[HinhThuc] ([LoaiHT])
GO
ALTER TABLE [dbo].[KhachHang] CHECK CONSTRAINT [FK_KhachHang_HinhThuc]
GO
/****** Object:  ForeignKey [FK_LichTrinh_TuyenXe2]    Script Date: 06/25/2017 11:42:13 ******/
ALTER TABLE [dbo].[LichTrinh]  WITH CHECK ADD  CONSTRAINT [FK_LichTrinh_TuyenXe2] FOREIGN KEY([MaTuyen])
REFERENCES [dbo].[TuyenXe] ([MaTuyen])
GO
ALTER TABLE [dbo].[LichTrinh] CHECK CONSTRAINT [FK_LichTrinh_TuyenXe2]
GO
/****** Object:  ForeignKey [FK_LichTrinh_XeKhach]    Script Date: 06/25/2017 11:42:13 ******/
ALTER TABLE [dbo].[LichTrinh]  WITH CHECK ADD  CONSTRAINT [FK_LichTrinh_XeKhach] FOREIGN KEY([SoXe])
REFERENCES [dbo].[XeKhach] ([SoXe])
GO
ALTER TABLE [dbo].[LichTrinh] CHECK CONSTRAINT [FK_LichTrinh_XeKhach]
GO
/****** Object:  ForeignKey [FK_NhanVien_LoaiNhanVien]    Script Date: 06/25/2017 11:42:13 ******/
ALTER TABLE [dbo].[NhanVien]  WITH CHECK ADD  CONSTRAINT [FK_NhanVien_LoaiNhanVien] FOREIGN KEY([LoaiNV])
REFERENCES [dbo].[LoaiNhanVien] ([MaLoaiNV])
GO
ALTER TABLE [dbo].[NhanVien] CHECK CONSTRAINT [FK_NhanVien_LoaiNhanVien]
GO
/****** Object:  ForeignKey [FK_PhanCong_TuyenXe]    Script Date: 06/25/2017 11:42:13 ******/
ALTER TABLE [dbo].[PhanCong]  WITH CHECK ADD  CONSTRAINT [FK_PhanCong_TuyenXe] FOREIGN KEY([MaTuyen])
REFERENCES [dbo].[TuyenXe] ([MaTuyen])
GO
ALTER TABLE [dbo].[PhanCong] CHECK CONSTRAINT [FK_PhanCong_TuyenXe]
GO
/****** Object:  ForeignKey [FK_TaiKhoan_LoaiTK]    Script Date: 06/25/2017 11:42:13 ******/
ALTER TABLE [dbo].[TaiKhoan]  WITH CHECK ADD  CONSTRAINT [FK_TaiKhoan_LoaiTK] FOREIGN KEY([LoaiTK])
REFERENCES [dbo].[LoaiTK] ([MaLoaiTK])
GO
ALTER TABLE [dbo].[TaiKhoan] CHECK CONSTRAINT [FK_TaiKhoan_LoaiTK]
GO
/****** Object:  ForeignKey [FK_VeXe_NhanVien]    Script Date: 06/25/2017 11:42:13 ******/
ALTER TABLE [dbo].[VeXe]  WITH CHECK ADD  CONSTRAINT [FK_VeXe_NhanVien] FOREIGN KEY([NhanVienBV])
REFERENCES [dbo].[NhanVien] ([MaNV])
GO
ALTER TABLE [dbo].[VeXe] CHECK CONSTRAINT [FK_VeXe_NhanVien]
GO
/****** Object:  ForeignKey [FK_VeXe_XeKhach]    Script Date: 06/25/2017 11:42:13 ******/
ALTER TABLE [dbo].[VeXe]  WITH CHECK ADD  CONSTRAINT [FK_VeXe_XeKhach] FOREIGN KEY([SoXe])
REFERENCES [dbo].[XeKhach] ([SoXe])
GO
ALTER TABLE [dbo].[VeXe] CHECK CONSTRAINT [FK_VeXe_XeKhach]
GO
