﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using QuanLyVe.Model;

namespace QuanLyVe.ViewModel
{
    public class ThongKeViewModel
    {
        BanVeEntities db = new BanVeEntities();
        public ObservableCollection<VeXe> getYear(int year)
        {
            var query = from s in db.VeXe
                        where s.NgayDi.Value.Year == year
                        select s;
            return new ObservableCollection<VeXe>(query.ToList());
        }
    }
}
