﻿using System.Collections.ObjectModel;
using System.Linq;
using QuanLyVe.Model;

namespace QuanLyVe.ViewModel
{
    public class ViewModelBase
    {
        BanVeEntities db = new BanVeEntities();

        public ObservableCollection<NhanVien> getEmployee()
        {
            //return new ObservableCollection<NhanVien>(db.NhanVien.ToList());
            return new ObservableCollection<NhanVien>(db.NhanVien.Where(w => w.BiXoa == "0").ToList());
        }

        public ObservableCollection<VeXe> getTickets()
        {
            return new ObservableCollection<VeXe>(db.VeXe.ToList());
        }

        public ObservableCollection<KhachHang> getCustomers()
        {
            return new ObservableCollection<KhachHang>(db.KhachHang.ToList());
        }

        public ObservableCollection<HangHoa> getWare()
        {
            return new ObservableCollection<HangHoa>(db.HangHoa.ToList());
        }

        public ObservableCollection<TuyenXe> getTuyenXe()
        {
            return new ObservableCollection<TuyenXe>(db.TuyenXe.ToList());
        }

        public ObservableCollection<LichTrinh> getLichTrinh()
        {
            return new ObservableCollection<LichTrinh>(db.LichTrinh.ToList());
        }

        public ObservableCollection<PhanCong> getPhanCong()
        {
            return new ObservableCollection<PhanCong>(db.PhanCong.ToList());
        }

    }
}
