﻿namespace QuanLyVe.ViewModel
{
    /// <summary>
    /// ISwitchable Members
    /// </summary>
  	public interface ISwitchable
    {
        void UtilizeState(object state);
    }
}