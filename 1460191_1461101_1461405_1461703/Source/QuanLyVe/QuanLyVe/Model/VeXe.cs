//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace QuanLyVe.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class VeXe
    {
        public string MaVe { get; set; }
        public string MaGhe { get; set; }
        public string MaKH { get; set; }
        public Nullable<System.DateTime> NgayDi { get; set; }
        public Nullable<System.TimeSpan> GioDi { get; set; }
        public string SoXe { get; set; }
        public Nullable<float> GiaVe { get; set; }
        public string NhanVienBV { get; set; }
    
        public virtual NhanVien NhanVien { get; set; }
        public virtual XeKhach XeKhach { get; set; }
    }
}
