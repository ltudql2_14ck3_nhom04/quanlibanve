﻿#pragma checksum "..\..\..\View\NVThongTinHangHoa.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "BF1CF2542D20C9523E0FD28F4854A367"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Transitions;
using QuanLyVe.View;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace QuanLyVe.View {
    
    
    /// <summary>
    /// NVThongTinHangHoa
    /// </summary>
    public partial class NVThongTinHangHoa : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 25 "..\..\..\View\NVThongTinHangHoa.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbSearch;
        
        #line default
        #line hidden
        
        
        #line 38 "..\..\..\View\NVThongTinHangHoa.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel stpSender;
        
        #line default
        #line hidden
        
        
        #line 39 "..\..\..\View\NVThongTinHangHoa.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbGui;
        
        #line default
        #line hidden
        
        
        #line 40 "..\..\..\View\NVThongTinHangHoa.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbGuiCMND;
        
        #line default
        #line hidden
        
        
        #line 41 "..\..\..\View\NVThongTinHangHoa.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbGuiDiaChi;
        
        #line default
        #line hidden
        
        
        #line 42 "..\..\..\View\NVThongTinHangHoa.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbGuiSDT;
        
        #line default
        #line hidden
        
        
        #line 47 "..\..\..\View\NVThongTinHangHoa.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel stpReceiver;
        
        #line default
        #line hidden
        
        
        #line 48 "..\..\..\View\NVThongTinHangHoa.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbNhan;
        
        #line default
        #line hidden
        
        
        #line 49 "..\..\..\View\NVThongTinHangHoa.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbNhanCMND;
        
        #line default
        #line hidden
        
        
        #line 50 "..\..\..\View\NVThongTinHangHoa.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbNhanDiaChi;
        
        #line default
        #line hidden
        
        
        #line 51 "..\..\..\View\NVThongTinHangHoa.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbNhanSDT;
        
        #line default
        #line hidden
        
        
        #line 62 "..\..\..\View\NVThongTinHangHoa.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid dtgHang;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/QuanLyVe;component/view/nvthongtinhanghoa.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\View\NVThongTinHangHoa.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 9 "..\..\..\View\NVThongTinHangHoa.xaml"
            ((QuanLyVe.View.NVThongTinHangHoa)(target)).Loaded += new System.Windows.RoutedEventHandler(this.UserControl_Loaded);
            
            #line default
            #line hidden
            return;
            case 2:
            
            #line 21 "..\..\..\View\NVThongTinHangHoa.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.btnSearch);
            
            #line default
            #line hidden
            return;
            case 3:
            this.tbSearch = ((System.Windows.Controls.TextBox)(target));
            
            #line 28 "..\..\..\View\NVThongTinHangHoa.xaml"
            this.tbSearch.PreviewKeyDown += new System.Windows.Input.KeyEventHandler(this.EnterKey);
            
            #line default
            #line hidden
            return;
            case 4:
            
            #line 30 "..\..\..\View\NVThongTinHangHoa.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.btnClear);
            
            #line default
            #line hidden
            return;
            case 5:
            this.stpSender = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 6:
            this.tbGui = ((System.Windows.Controls.TextBox)(target));
            return;
            case 7:
            this.tbGuiCMND = ((System.Windows.Controls.TextBox)(target));
            return;
            case 8:
            this.tbGuiDiaChi = ((System.Windows.Controls.TextBox)(target));
            return;
            case 9:
            this.tbGuiSDT = ((System.Windows.Controls.TextBox)(target));
            return;
            case 10:
            this.stpReceiver = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 11:
            this.tbNhan = ((System.Windows.Controls.TextBox)(target));
            return;
            case 12:
            this.tbNhanCMND = ((System.Windows.Controls.TextBox)(target));
            return;
            case 13:
            this.tbNhanDiaChi = ((System.Windows.Controls.TextBox)(target));
            return;
            case 14:
            this.tbNhanSDT = ((System.Windows.Controls.TextBox)(target));
            return;
            case 15:
            
            #line 56 "..\..\..\View\NVThongTinHangHoa.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.btnUpdate);
            
            #line default
            #line hidden
            return;
            case 16:
            
            #line 57 "..\..\..\View\NVThongTinHangHoa.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.btnSave);
            
            #line default
            #line hidden
            return;
            case 17:
            this.dtgHang = ((System.Windows.Controls.DataGrid)(target));
            
            #line 62 "..\..\..\View\NVThongTinHangHoa.xaml"
            this.dtgHang.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.dtgHang_SelectionChanged);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

