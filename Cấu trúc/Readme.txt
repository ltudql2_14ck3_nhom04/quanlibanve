Mã nhóm: 04
Lớp 14CK3

Thông tin liên lạc

1. Đỗ Thị Mai Trúc - 1461703
SĐT: 01644352231
2. Phạm Đoàn Hoàng Dung - 1460191
SĐT: 0932258700
3. Nguyễn Thị Khánh - 1461405
SĐT: 01644460699
4. Phạm Thị Ngọc Tuyết - 1461101
SĐT: 0971418313

Hướng dẫn cài đặt
1. Chạy script CSDL bên trong thư mục Source
2. Chỉnh đường dẫn kết nối với CSDL trong thư mục App.config
Tên đăng nhập và mật khẩu (thuộc tính TenDN trong và MatKhau trong bảng Tài Khoản)
Mặc định (ứng dụng có 2 phân hệ là quản lí hoặc là nhân viên bán vé)
Nếu là quản lí có thể đăng nhập với tên đăng nhập là: quanli ; mật khẩu là: 000000
Nếu là nhân viên bán vé có thể đăng nhập với tên đăng nhập là: nhanvien ; mật khẩu là: 000000