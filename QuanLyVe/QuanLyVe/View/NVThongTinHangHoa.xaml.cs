﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Collections.ObjectModel;
using QuanLyVe.Model;

namespace QuanLyVe.View
{
    /// <summary>
    /// Interaction logic for NVThongTinHangHoa.xaml
    /// </summary>
    public partial class NVThongTinHangHoa : UserControl
    {
        public NVThongTinHangHoa()
        {
            InitializeComponent();
        }

        BanVeEntities db = new BanVeEntities();
        private ObservableCollection<HangHoa> getHangHoa()
        {
            return new ObservableCollection<HangHoa>(db.HangHoa.ToList());
        }

        private void Search()
        {
            var search = from b in db.HangHoa
                         where b.MaHH.Contains(tbSearch.Text)
                         select b;
            ObservableCollection<HangHoa> data = new ObservableCollection<HangHoa>(search.ToList());
            dtgHang.ItemsSource = data;
        }

        private void ClearAll()
        {
            foreach (UIElement c in stpSender.Children)
                if (c is TextBox)
                {
                    TextBox tb = c as TextBox;
                    tb.Clear();
                }

            foreach (UIElement c in stpReceiver.Children)
                if (c is TextBox)
                {
                    TextBox tb = c as TextBox;
                    tb.Clear();
                }
        }

        private void setEnable()
        {
            foreach (UIElement c in stpSender.Children)
                if (c is TextBox)
                {
                    TextBox tb = c as TextBox;
                    tb.IsReadOnly = false;
                }

            foreach (UIElement c in stpReceiver.Children)
                if (c is TextBox)
                {
                    TextBox tb = c as TextBox;
                    tb.IsReadOnly = false;
                }
        }

        private void setReadOnly()
        {
            foreach (UIElement c in stpSender.Children)
                if (c is TextBox)
                {
                    TextBox tb = c as TextBox;
                    tb.IsReadOnly = true;
                }

            foreach (UIElement c in stpReceiver.Children)
                if (c is TextBox)
                {
                    TextBox tb = c as TextBox;
                    tb.IsReadOnly = true;
                }
        }

        private void btnSearch(object sender, RoutedEventArgs e)
        {
            Search();
        }

        private void btnClear(object sender, RoutedEventArgs e)
        {
            tbSearch.Clear();
            dtgHang.ItemsSource = getHangHoa();
        }

        private void EnterKey(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                Search();
                e.Handled = true;
            }
        }

        private void btnUpdate(object sender, RoutedEventArgs e)
        {
            if (dtgHang.SelectedItem == null)
                MessageBox.Show("Chọn trước khi sửa", "Thông báo", MessageBoxButton.OK, MessageBoxImage.None);
            else
            {
                var question = MessageBox.Show("Bạn muốn thay đổi thông tin khách hàng?", "Xác nhận", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (question == MessageBoxResult.Yes)
                {
                    setEnable();
                }
            }
        }

        private void btnSave(object sender, RoutedEventArgs e)
        {
            if (dtgHang.SelectedItem == null)
                MessageBox.Show("Chọn trước khi lưu", "Thông báo", MessageBoxButton.OK, MessageBoxImage.None);
            else
            {
                var selected = dtgHang.SelectedItem as HangHoa;

                var question = MessageBox.Show("Lưu thông tin khách hàng?", "Xác nhận", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (question == MessageBoxResult.Yes)
                {
                    KhachHang _sender = db.KhachHang.Find(selected.NguoiGui);
                    // thay doi nguoi gui
                    _sender.TenKH = tbGui.Text;
                    _sender.CMND = tbGuiCMND.Text;
                    _sender.DiaChi = tbGuiDiaChi.Text;
                    _sender.SDT = tbGuiSDT.Text;

                    db.SaveChanges();

                    //thay doi nguoi nhan
                    KhachHang _receiver = db.KhachHang.Find(selected.NguoiGui);
                    // thay doi nguoi gui
                    _receiver.TenKH = tbNhan.Text;
                    _receiver.CMND = tbNhanCMND.Text;
                    _receiver.DiaChi = tbNhanDiaChi.Text;
                    _receiver.SDT = tbNhanSDT.Text;

                    db.SaveChanges();
                    ClearAll();
                    setReadOnly();
                }
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            dtgHang.ItemsSource = getHangHoa();
        }

        private void dtgHang_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dtgHang.SelectedItem != null)
            {
                // lấy người nhận người gửi từ database
                var selected = dtgHang.SelectedItem as HangHoa;
                KhachHang _sender = db.KhachHang.Where(s => s.MaKH == selected.NguoiGui).SingleOrDefault();
                KhachHang _receiver = db.KhachHang.Where(s => s.MaKH == selected.NguoiNhan).SingleOrDefault();

                //binding
                tbGui.Text = _sender.TenKH;
                tbGuiCMND.Text = _sender.CMND;
                tbGuiDiaChi.Text = _sender.DiaChi;
                tbGuiSDT.Text = _sender.SDT;

                tbNhan.Text = _receiver.TenKH;
                tbNhanCMND.Text = _receiver.CMND;
                tbNhanDiaChi.Text = _receiver.DiaChi;
                tbNhanSDT.Text = _receiver.SDT;
            }
        }
    }
}
