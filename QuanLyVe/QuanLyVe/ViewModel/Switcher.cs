﻿using QuanLyVe.View;
using System.Windows.Controls;

namespace QuanLyVe.ViewModel
{
    /// <summary>
    /// Switcher Base Class
    /// </summary>
    public static class Switcher
    {
        public static ViewSwitcher viewSwitcher;

        public static void Switch(UserControl newPage)
        {
            viewSwitcher.Navigate(newPage);
        }
    }
}
