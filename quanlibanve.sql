USE [quanlivexe1]
GO
/****** Object:  Table [dbo].[Ghe]    Script Date: 15/04/2017 11:43:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ghe](
	[MaGhe] [nchar](10) NOT NULL,
	[MaTuyen] [nchar](10) NULL,
	[MaKH] [nchar](10) NULL,
	[TinhTrang] [nvarchar](50) NULL,
	[SoXe] [nchar](15) NULL,
 CONSTRAINT [PK_Ghe] PRIMARY KEY CLUSTERED 
(
	[MaGhe] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HangHoa]    Script Date: 15/04/2017 11:43:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HangHoa](
	[MaHH] [nchar](10) NOT NULL,
	[LoaiHH] [nchar](10) NULL,
	[KhoiLuong] [float] NULL,
	[Phi] [real] NULL,
	[NguoiGui] [nchar](10) NULL,
	[NguoiNhan] [nchar](10) NULL,
	[NhanVienNhan] [nchar](10) NULL,
 CONSTRAINT [PK_HangHoa] PRIMARY KEY CLUSTERED 
(
	[MaHH] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HinhThuc]    Script Date: 15/04/2017 11:43:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HinhThuc](
	[LoaiHT] [nchar](10) NOT NULL,
	[TenHT] [nvarchar](50) NULL,
 CONSTRAINT [PK_HinhThuc] PRIMARY KEY CLUSTERED 
(
	[LoaiHT] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HoaDonHH]    Script Date: 15/04/2017 11:43:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HoaDonHH](
	[MaHD] [nchar](10) NOT NULL,
	[MaHH] [nchar](10) NULL,
	[MaKH] [nchar](10) NULL,
	[TongTien] [real] NULL,
 CONSTRAINT [PK_HoaDonHH] PRIMARY KEY CLUSTERED 
(
	[MaHD] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HoaDonVe]    Script Date: 15/04/2017 11:43:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HoaDonVe](
	[MaHD] [nchar](10) NULL,
	[MaVe] [nchar](10) NULL,
	[MaKH] [nchar](10) NULL,
	[SoLuong] [int] NULL,
	[TongTien] [real] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[KhachHang]    Script Date: 15/04/2017 11:43:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KhachHang](
	[MaKH] [nchar](10) NOT NULL,
	[TenKH] [nvarchar](50) NULL,
	[CMND] [nchar](15) NULL,
	[DiaChi] [nvarchar](50) NULL,
	[SDT] [nchar](15) NULL,
	[HinhThuc] [nchar](10) NULL,
 CONSTRAINT [PK_KhachHang] PRIMARY KEY CLUSTERED 
(
	[MaKH] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LichTrinh]    Script Date: 15/04/2017 11:43:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LichTrinh](
	[MaTuyen] [nchar](10) NOT NULL,
	[NgayDi] [date] NOT NULL,
	[NgayDen] [date] NOT NULL,
	[SoXe] [nchar](15) NOT NULL,
 CONSTRAINT [PK_LichTrinh] PRIMARY KEY CLUSTERED 
(
	[MaTuyen] ASC,
	[NgayDi] ASC,
	[NgayDen] ASC,
	[SoXe] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LoaiNhanVien]    Script Date: 15/04/2017 11:43:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoaiNhanVien](
	[MaLoaiNV] [nchar](10) NOT NULL,
	[TenLoai] [nvarchar](50) NULL,
 CONSTRAINT [PK_LoaiNhanVien] PRIMARY KEY CLUSTERED 
(
	[MaLoaiNV] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LoaiTK]    Script Date: 15/04/2017 11:43:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoaiTK](
	[MaLoaiTK] [nchar](10) NOT NULL,
	[TenLoai] [nvarchar](50) NULL,
 CONSTRAINT [PK_LoaiTK] PRIMARY KEY CLUSTERED 
(
	[MaLoaiTK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NhanVien]    Script Date: 15/04/2017 11:43:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NhanVien](
	[MaNV] [nchar](10) NOT NULL,
	[TenNV] [nvarchar](50) NULL,
	[NgaySinh] [date] NULL,
	[CMND] [nchar](15) NULL,
	[DiaChi] [nvarchar](50) NULL,
	[SDT] [nchar](15) NULL,
	[LoaiNV] [nchar](10) NULL,
 CONSTRAINT [PK_NhanVien] PRIMARY KEY CLUSTERED 
(
	[MaNV] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PhanCong]    Script Date: 15/04/2017 11:43:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PhanCong](
	[MaNV] [nchar](10) NOT NULL,
	[NgayDi] [date] NULL,
	[MaTuyen] [nchar](10) NULL,
 CONSTRAINT [PK_PhanCong] PRIMARY KEY CLUSTERED 
(
	[MaNV] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TaiKhoan]    Script Date: 15/04/2017 11:43:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TaiKhoan](
	[TenDN] [nvarchar](50) NOT NULL,
	[MatKhau] [nchar](10) NOT NULL,
	[LoaiTK] [nchar](10) NULL,
 CONSTRAINT [PK_TaiKhoan] PRIMARY KEY CLUSTERED 
(
	[TenDN] ASC,
	[MatKhau] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TuyenXe]    Script Date: 15/04/2017 11:43:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TuyenXe](
	[MaTuyen] [nchar](10) NOT NULL,
	[DiaDiemDi] [nvarchar](50) NULL,
	[DiaDiemDen] [nvarchar](50) NULL,
	[GioDi] [time](7) NULL,
	[GioDen] [time](7) NULL,
 CONSTRAINT [PK_TuyenXe] PRIMARY KEY CLUSTERED 
(
	[MaTuyen] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[VeXe]    Script Date: 15/04/2017 11:43:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VeXe](
	[MaVe] [nchar](10) NOT NULL,
	[MaGhe] [nchar](10) NULL,
	[MaKH] [nchar](10) NULL,
	[NgayDi] [date] NULL,
	[GioDi] [time](7) NULL,
	[SoXe] [nchar](15) NULL,
	[GiaVe] [real] NULL,
	[NhanVienBV] [nchar](10) NULL,
 CONSTRAINT [PK_VeXe] PRIMARY KEY CLUSTERED 
(
	[MaVe] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[XeKhach]    Script Date: 15/04/2017 11:43:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[XeKhach](
	[SoXe] [nchar](15) NOT NULL,
	[HangSX] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_XeKhach] PRIMARY KEY CLUSTERED 
(
	[SoXe] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
INSERT [dbo].[Ghe] ([MaGhe], [MaTuyen], [MaKH], [TinhTrang], [SoXe]) VALUES (N'GH001     ', N'T001      ', N'KH001     ', N'Đã đặt', N'10B4-32435     ')
GO
INSERT [dbo].[Ghe] ([MaGhe], [MaTuyen], [MaKH], [TinhTrang], [SoXe]) VALUES (N'GH002     ', N'T001      ', N'KH002     ', N'Đã đặt', N'10B4-32435     ')
GO
INSERT [dbo].[Ghe] ([MaGhe], [MaTuyen], [MaKH], [TinhTrang], [SoXe]) VALUES (N'GH003     ', N'T001      ', NULL, N'Trống', N'10B4-32435     ')
GO
INSERT [dbo].[Ghe] ([MaGhe], [MaTuyen], [MaKH], [TinhTrang], [SoXe]) VALUES (N'GH004     ', N'T001      ', N'KH005     ', N'Đã đặt', N'10B4-32435     ')
GO
INSERT [dbo].[Ghe] ([MaGhe], [MaTuyen], [MaKH], [TinhTrang], [SoXe]) VALUES (N'GH005     ', N'T001      ', N'KH005     ', N'Đã đặt', N'10B4-32435     ')
GO
INSERT [dbo].[Ghe] ([MaGhe], [MaTuyen], [MaKH], [TinhTrang], [SoXe]) VALUES (N'GH006     ', N'T001      ', N'KH008     ', N'Đã đặt', N'10B4-32435     ')
GO
INSERT [dbo].[Ghe] ([MaGhe], [MaTuyen], [MaKH], [TinhTrang], [SoXe]) VALUES (N'GH007     ', N'T001      ', N'KH008     ', N'Đã đặt', N'10B4-32435     ')
GO
INSERT [dbo].[Ghe] ([MaGhe], [MaTuyen], [MaKH], [TinhTrang], [SoXe]) VALUES (N'GH010     ', N'T002      ', N'KH009     ', N'Đã đặt', N'25B4-47832     ')
GO
INSERT [dbo].[Ghe] ([MaGhe], [MaTuyen], [MaKH], [TinhTrang], [SoXe]) VALUES (N'GH020     ', N'T002      ', N'KH009     ', N'Đã đặt', N'25B4-47832     ')
GO
INSERT [dbo].[Ghe] ([MaGhe], [MaTuyen], [MaKH], [TinhTrang], [SoXe]) VALUES (N'GH030     ', N'T002      ', N'KH009     ', N'Đã đặt', N'25B4-47832     ')
GO
INSERT [dbo].[Ghe] ([MaGhe], [MaTuyen], [MaKH], [TinhTrang], [SoXe]) VALUES (N'GH040     ', N'T002      ', NULL, N'Trống', N'25B4-47832     ')
GO
INSERT [dbo].[Ghe] ([MaGhe], [MaTuyen], [MaKH], [TinhTrang], [SoXe]) VALUES (N'GH050     ', N'T002      ', NULL, N'Trống', N'25B4-47832     ')
GO
INSERT [dbo].[Ghe] ([MaGhe], [MaTuyen], [MaKH], [TinhTrang], [SoXe]) VALUES (N'GH060     ', N'T002      ', NULL, N'Trống', N'25B4-47832     ')
GO
INSERT [dbo].[Ghe] ([MaGhe], [MaTuyen], [MaKH], [TinhTrang], [SoXe]) VALUES (N'GH070     ', N'T002      ', N'KH010     ', N'Đã đặt', N'25B4-47832     ')
GO
INSERT [dbo].[Ghe] ([MaGhe], [MaTuyen], [MaKH], [TinhTrang], [SoXe]) VALUES (N'GH100     ', N'T004      ', N'KH013     ', N'Đã đặt', N'56B2-89110     ')
GO
INSERT [dbo].[Ghe] ([MaGhe], [MaTuyen], [MaKH], [TinhTrang], [SoXe]) VALUES (N'GH110     ', N'T003      ', NULL, N'Trống', N'78B3-22115     ')
GO
INSERT [dbo].[Ghe] ([MaGhe], [MaTuyen], [MaKH], [TinhTrang], [SoXe]) VALUES (N'GH120     ', N'T003      ', NULL, N'Trống', N'78B3-22115     ')
GO
INSERT [dbo].[Ghe] ([MaGhe], [MaTuyen], [MaKH], [TinhTrang], [SoXe]) VALUES (N'GH130     ', N'T003      ', NULL, N'Trống', N'78B3-22115     ')
GO
INSERT [dbo].[Ghe] ([MaGhe], [MaTuyen], [MaKH], [TinhTrang], [SoXe]) VALUES (N'GH140     ', N'T003      ', N'KH019     ', N'Đã đặt', N'78B3-22115     ')
GO
INSERT [dbo].[Ghe] ([MaGhe], [MaTuyen], [MaKH], [TinhTrang], [SoXe]) VALUES (N'GH200     ', N'T004      ', N'KH015     ', N'Đã đặt', N'56B2-89110     ')
GO
INSERT [dbo].[Ghe] ([MaGhe], [MaTuyen], [MaKH], [TinhTrang], [SoXe]) VALUES (N'GH300     ', N'T004      ', N'KH015     ', N'Đã đặt', N'56B2-89110     ')
GO
INSERT [dbo].[Ghe] ([MaGhe], [MaTuyen], [MaKH], [TinhTrang], [SoXe]) VALUES (N'GH400     ', N'T004      ', N'KH016     ', N'Đã đặt', N'56B2-89110     ')
GO
INSERT [dbo].[Ghe] ([MaGhe], [MaTuyen], [MaKH], [TinhTrang], [SoXe]) VALUES (N'GH500     ', N'T004      ', N'KH016     ', N'Đã đặt', N'56B2-89110     ')
GO
INSERT [dbo].[Ghe] ([MaGhe], [MaTuyen], [MaKH], [TinhTrang], [SoXe]) VALUES (N'GH600     ', N'T004      ', N'KH013     ', N'Đã đặt', N'56B2-89110     ')
GO
INSERT [dbo].[Ghe] ([MaGhe], [MaTuyen], [MaKH], [TinhTrang], [SoXe]) VALUES (N'GH700     ', N'T004      ', NULL, N'Trống', N'56B2-89110     ')
GO
INSERT [dbo].[HangHoa] ([MaHH], [LoaiHH], [KhoiLuong], [Phi], [NguoiGui], [NguoiNhan], [NhanVienNhan]) VALUES (N'HH001     ', N'Hàng dễ vỡ', 30, 20000, N'KH001     ', N'KH005     ', N'NV001     ')
GO
INSERT [dbo].[HangHoa] ([MaHH], [LoaiHH], [KhoiLuong], [Phi], [NguoiGui], [NguoiNhan], [NhanVienNhan]) VALUES (N'HH002     ', N'Xe máy    ', 40, 20000, N'KH002     ', N'KH011     ', N'NV002     ')
GO
INSERT [dbo].[HangHoa] ([MaHH], [LoaiHH], [KhoiLuong], [Phi], [NguoiGui], [NguoiNhan], [NhanVienNhan]) VALUES (N'HH003     ', N'Giấy tờ   ', 1, 10000, N'KH003     ', N'KH020     ', N'NV003     ')
GO
INSERT [dbo].[HangHoa] ([MaHH], [LoaiHH], [KhoiLuong], [Phi], [NguoiGui], [NguoiNhan], [NhanVienNhan]) VALUES (N'HH004     ', N'Thực phẩm ', 5, 20000, N'KH004     ', N'KH009     ', N'NV004     ')
GO
INSERT [dbo].[HangHoa] ([MaHH], [LoaiHH], [KhoiLuong], [Phi], [NguoiGui], [NguoiNhan], [NhanVienNhan]) VALUES (N'HH005     ', N'Hàng dễ vỡ', 10, 20000, N'KH005     ', N'KH002     ', N'NV005     ')
GO
INSERT [dbo].[HangHoa] ([MaHH], [LoaiHH], [KhoiLuong], [Phi], [NguoiGui], [NguoiNhan], [NhanVienNhan]) VALUES (N'HH006     ', N'Đồ điện tử', 5, 20000, N'KH006     ', N'KH001     ', N'NV006     ')
GO
INSERT [dbo].[HangHoa] ([MaHH], [LoaiHH], [KhoiLuong], [Phi], [NguoiGui], [NguoiNhan], [NhanVienNhan]) VALUES (N'HH007     ', N'Giấy tờ   ', 1, 10000, N'KH007     ', N'KH012     ', N'NV007     ')
GO
INSERT [dbo].[HangHoa] ([MaHH], [LoaiHH], [KhoiLuong], [Phi], [NguoiGui], [NguoiNhan], [NhanVienNhan]) VALUES (N'HH008     ', N'Thực phẩm ', 20, 20000, N'KH008     ', N'KH019     ', N'NV008     ')
GO
INSERT [dbo].[HangHoa] ([MaHH], [LoaiHH], [KhoiLuong], [Phi], [NguoiGui], [NguoiNhan], [NhanVienNhan]) VALUES (N'HH009     ', N'Xe máy    ', 40, 20000, N'KH009     ', N'KH004     ', N'NV009     ')
GO
INSERT [dbo].[HangHoa] ([MaHH], [LoaiHH], [KhoiLuong], [Phi], [NguoiGui], [NguoiNhan], [NhanVienNhan]) VALUES (N'HH010     ', N'Hàng dễ vỡ', 10, 20000, N'KH010     ', N'KH003     ', N'NV010     ')
GO
INSERT [dbo].[HangHoa] ([MaHH], [LoaiHH], [KhoiLuong], [Phi], [NguoiGui], [NguoiNhan], [NhanVienNhan]) VALUES (N'HH011     ', N'Giấy tờ   ', 1, 10000, N'KH011     ', N'KH002     ', N'NV011     ')
GO
INSERT [dbo].[HangHoa] ([MaHH], [LoaiHH], [KhoiLuong], [Phi], [NguoiGui], [NguoiNhan], [NhanVienNhan]) VALUES (N'HH012     ', N'Đồ điện tử', 5, 20000, N'KH012     ', N'KH005     ', N'NV012     ')
GO
INSERT [dbo].[HangHoa] ([MaHH], [LoaiHH], [KhoiLuong], [Phi], [NguoiGui], [NguoiNhan], [NhanVienNhan]) VALUES (N'HH013     ', N'Giấy tờ   ', 2, 10000, N'KH013     ', N'KH006     ', N'NV013     ')
GO
INSERT [dbo].[HangHoa] ([MaHH], [LoaiHH], [KhoiLuong], [Phi], [NguoiGui], [NguoiNhan], [NhanVienNhan]) VALUES (N'HH014     ', N'Thực phẩm ', 40, 20000, N'KH014     ', N'KH007     ', N'NV014     ')
GO
INSERT [dbo].[HangHoa] ([MaHH], [LoaiHH], [KhoiLuong], [Phi], [NguoiGui], [NguoiNhan], [NhanVienNhan]) VALUES (N'HH015     ', N'Hàng dễ vỡ', 76, 20000, N'KH015     ', N'KH018     ', N'NV015     ')
GO
INSERT [dbo].[HangHoa] ([MaHH], [LoaiHH], [KhoiLuong], [Phi], [NguoiGui], [NguoiNhan], [NhanVienNhan]) VALUES (N'HH016     ', N'Giấy tờ   ', 2, 10000, N'KH016     ', N'KH010     ', N'NV016     ')
GO
INSERT [dbo].[HangHoa] ([MaHH], [LoaiHH], [KhoiLuong], [Phi], [NguoiGui], [NguoiNhan], [NhanVienNhan]) VALUES (N'HH017     ', N'Xe máy    ', 20, 20000, N'KH017     ', N'KH017     ', N'NV017     ')
GO
INSERT [dbo].[HangHoa] ([MaHH], [LoaiHH], [KhoiLuong], [Phi], [NguoiGui], [NguoiNhan], [NhanVienNhan]) VALUES (N'HH018     ', N'Thực phẩm ', 30, 20000, N'KH018     ', N'KH006     ', N'NV018     ')
GO
INSERT [dbo].[HangHoa] ([MaHH], [LoaiHH], [KhoiLuong], [Phi], [NguoiGui], [NguoiNhan], [NhanVienNhan]) VALUES (N'HH019     ', N'Giấy tờ   ', 2, 10000, N'KH019     ', N'KH012     ', N'NV019     ')
GO
INSERT [dbo].[HangHoa] ([MaHH], [LoaiHH], [KhoiLuong], [Phi], [NguoiGui], [NguoiNhan], [NhanVienNhan]) VALUES (N'HH020     ', N'Giấy tờ   ', 3, 10000, N'KH020     ', N'KH007     ', N'NV020     ')
GO
INSERT [dbo].[HinhThuc] ([LoaiHT], [TenHT]) VALUES (N'H01       ', N'Đi xe')
GO
INSERT [dbo].[HinhThuc] ([LoaiHT], [TenHT]) VALUES (N'H02       ', N'Gửi Hàng')
GO
INSERT [dbo].[HinhThuc] ([LoaiHT], [TenHT]) VALUES (N'H03       ', N'Nhận Hàng')
GO
INSERT [dbo].[HoaDonHH] ([MaHD], [MaHH], [MaKH], [TongTien]) VALUES (N'HD004     ', N'HH004     ', N'KH004     ', 100000)
GO
INSERT [dbo].[HoaDonHH] ([MaHD], [MaHH], [MaKH], [TongTien]) VALUES (N'HD006     ', N'HH006     ', N'KH006     ', 100000)
GO
INSERT [dbo].[HoaDonHH] ([MaHD], [MaHH], [MaKH], [TongTien]) VALUES (N'HD011     ', N'HH011     ', N'KH011     ', 10000)
GO
INSERT [dbo].[HoaDonHH] ([MaHD], [MaHH], [MaKH], [TongTien]) VALUES (N'HD014     ', N'HH014     ', N'KH014     ', 80000)
GO
INSERT [dbo].[HoaDonHH] ([MaHD], [MaHH], [MaKH], [TongTien]) VALUES (N'HD018     ', N'HH018     ', N'KH018     ', 600000)
GO
INSERT [dbo].[HoaDonVe] ([MaHD], [MaVe], [MaKH], [SoLuong], [TongTien]) VALUES (N'HV001     ', N'V020      ', N'KH018     ', 0, 0)
GO
INSERT [dbo].[HoaDonVe] ([MaHD], [MaVe], [MaKH], [SoLuong], [TongTien]) VALUES (N'HV002     ', N'V019      ', N'KH019     ', 1, 400000)
GO
INSERT [dbo].[HoaDonVe] ([MaHD], [MaVe], [MaKH], [SoLuong], [TongTien]) VALUES (N'HV003     ', N'V018      ', N'KH016     ', 2, 100000)
GO
INSERT [dbo].[HoaDonVe] ([MaHD], [MaVe], [MaKH], [SoLuong], [TongTien]) VALUES (N'HV004     ', N'V017      ', N'KH017     ', 0, 0)
GO
INSERT [dbo].[HoaDonVe] ([MaHD], [MaVe], [MaKH], [SoLuong], [TongTien]) VALUES (N'HV005     ', N'V016      ', N'KH020     ', 0, 0)
GO
INSERT [dbo].[HoaDonVe] ([MaHD], [MaVe], [MaKH], [SoLuong], [TongTien]) VALUES (N'HV006     ', N'V015      ', N'KH014     ', 0, 0)
GO
INSERT [dbo].[HoaDonVe] ([MaHD], [MaVe], [MaKH], [SoLuong], [TongTien]) VALUES (N'HV007     ', N'V014      ', N'KH015     ', 2, 800000)
GO
INSERT [dbo].[HoaDonVe] ([MaHD], [MaVe], [MaKH], [SoLuong], [TongTien]) VALUES (N'HV008     ', N'V013      ', N'KH012     ', 0, 0)
GO
INSERT [dbo].[HoaDonVe] ([MaHD], [MaVe], [MaKH], [SoLuong], [TongTien]) VALUES (N'HV009     ', N'V012      ', N'KH013     ', 2, 240000)
GO
INSERT [dbo].[HoaDonVe] ([MaHD], [MaVe], [MaKH], [SoLuong], [TongTien]) VALUES (N'HV010     ', N'V011      ', N'KH010     ', 1, 200000)
GO
INSERT [dbo].[HoaDonVe] ([MaHD], [MaVe], [MaKH], [SoLuong], [TongTien]) VALUES (N'HV011     ', N'V010      ', N'KH011     ', 0, 0)
GO
INSERT [dbo].[HoaDonVe] ([MaHD], [MaVe], [MaKH], [SoLuong], [TongTien]) VALUES (N'HV012     ', N'V009      ', N'KH006     ', 0, 0)
GO
INSERT [dbo].[HoaDonVe] ([MaHD], [MaVe], [MaKH], [SoLuong], [TongTien]) VALUES (N'HV013     ', N'V008      ', N'KH008     ', 2, 600000)
GO
INSERT [dbo].[HoaDonVe] ([MaHD], [MaVe], [MaKH], [SoLuong], [TongTien]) VALUES (N'HV014     ', N'V007      ', N'KH007     ', 0, 0)
GO
INSERT [dbo].[HoaDonVe] ([MaHD], [MaVe], [MaKH], [SoLuong], [TongTien]) VALUES (N'HV015     ', N'V006      ', N'KH002     ', 1, 200000)
GO
INSERT [dbo].[HoaDonVe] ([MaHD], [MaVe], [MaKH], [SoLuong], [TongTien]) VALUES (N'HV016     ', N'V005      ', N'KH004     ', 0, 0)
GO
INSERT [dbo].[HoaDonVe] ([MaHD], [MaVe], [MaKH], [SoLuong], [TongTien]) VALUES (N'HV017     ', N'V004      ', N'KH001     ', 1, 150000)
GO
INSERT [dbo].[HoaDonVe] ([MaHD], [MaVe], [MaKH], [SoLuong], [TongTien]) VALUES (N'HV018     ', N'V003      ', N'KH003     ', 0, 0)
GO
INSERT [dbo].[HoaDonVe] ([MaHD], [MaVe], [MaKH], [SoLuong], [TongTien]) VALUES (N'HV019     ', N'V002      ', N'KH009     ', 3, 300000)
GO
INSERT [dbo].[HoaDonVe] ([MaHD], [MaVe], [MaKH], [SoLuong], [TongTien]) VALUES (N'HV020     ', N'V001      ', N'KH005     ', 2, 100000)
GO
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [HinhThuc]) VALUES (N'KH001     ', N'Trần Tầm', N'264412313      ', N'12 Nguyễn Chí Thanh, Phường 10, Quận 11', N'09122344620    ', N'H01       ')
GO
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [HinhThuc]) VALUES (N'KH002     ', N'Đường Thần Duệ', N'264425461      ', N'Khánh Hội, Ninh Hải, Ninh Thuận', N'01684151823    ', N'H01       ')
GO
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [HinhThuc]) VALUES (N'KH003     ', N' Tuỳ Vũ Nhi An', N'264123256      ', N'AH17, Kim Dinh, Tp. Bà Rịa, Bà Rịa - Vũng Tàu', N'01664526524    ', N'H03       ')
GO
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [HinhThuc]) VALUES (N'KH004     ', N'Tịch Si Thần', N'264597853      ', N' 7/7 Hoàng Đạo Thành, Thanh Xuân, Hà Nội', N'09621543225    ', N'H02       ')
GO
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [HinhThuc]) VALUES (N'KH005     ', N'Cố Duật Hành', N'321563548      ', N' QL1A, Tân Lập, Hàm Thuận Nam, Bình Thuận', N'01235486236    ', N'H01       ')
GO
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [HinhThuc]) VALUES (N'KH006     ', N'Lâm Dương', N'125633256      ', N'32 Nguyễn Tất Thành, Thị xã Hương Thủy, Huế', N'01652458633    ', N'H02       ')
GO
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [HinhThuc]) VALUES (N'KH007     ', N'Quách Kính Minh', N'245896324      ', N'25/12 Trần Hưng Đạo, tp.Hội An, Đà Nẵng', N'01664751283    ', N'H03       ')
GO
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [HinhThuc]) VALUES (N'KH008     ', N'Dư Mặc', N'524632893      ', N'44 Nguyễn Bỉnh Khiêm, Xương Huân, Tp. Nha Trang', N'01689532456    ', N'H01       ')
GO
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [HinhThuc]) VALUES (N'KH009     ', N'Phương Dư Khả', N'345682632      ', N'67 Nguyễn Trãi
Phường 9,
Tp. Đà Lạt, 
Lâm Đồng', N'09235468965    ', N'H01       ')
GO
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [HinhThuc]) VALUES (N'KH010     ', N'Lâm Khải Chính', N'246523586      ', N'534 Thiên Lôi, 
Vĩnh Niệm
, Lê Chân, 
Hải Phòng', N'09625422356    ', N'H01       ')
GO
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [HinhThuc]) VALUES (N'KH011     ', N'Hàn Tiềm', N'542632563      ', N'125/2 Đinh Bộ Lĩnh, Phường 13, Quận Bình Thạnh', N'01695232457    ', N'H02       ')
GO
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [HinhThuc]) VALUES (N'KH012     ', N'La Đông Phong ', N'265896325      ', N'Tri Thủy, Ninh Hải, Ninh Thuận', N'09625468310    ', N'H03       ')
GO
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [HinhThuc]) VALUES (N'KH013     ', N'Hoắc Triển Bạch', N'356847962      ', N'51 Phạm Văn Đồng, TP.Nha Trang, Khánh Hòa', N'01658469321    ', N'H01       ')
GO
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [HinhThuc]) VALUES (N'KH014     ', N'Mạc Thiệu Khiêm', N'256489353      ', N'115 Nguyễn Đình Chiểu, Phường 12, Quận 3, Tp.HCM', N'09615236548    ', N'H02       ')
GO
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [HinhThuc]) VALUES (N'KH015     ', N'Lý Thừa Ngân', N'652345220      ', N'65 Quang Trung, Phường 5, Gò Vấp, Tp.HCM', N'01684523659    ', N'H01       ')
GO
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [HinhThuc]) VALUES (N'KH016     ', N'Dạ Thiên Lăng', N'24569856       ', N'18 Nguyễn Văn Cừ, phường 3, Quận 5, Tp.HCM', N'09125489652    ', N'H01       ')
GO
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [HinhThuc]) VALUES (N'KH017     ', N'Tề Mặc', N'54589623       ', N'264 Huỳnh Văn Cù, Tp.Thủ Dầu Một, Bình Dương', N'01685459630    ', N'H01       ')
GO
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [HinhThuc]) VALUES (N'KH018     ', N'Vũ Văn Duệ', N'24596852       ', N'256 Cách Mạng Tháng 8, Phường 5, Quận 3, Tp.HCM', N'09652352632    ', N'H02       ')
GO
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [HinhThuc]) VALUES (N'KH019     ', N'Lý Mộc Ngư', N'65869631       ', N'26 Điện Biên Phủ, P.12, Quận Bình Thạnh, Tp.HCM', N'01685326995    ', N'H01       ')
GO
INSERT [dbo].[KhachHang] ([MaKH], [TenKH], [CMND], [DiaChi], [SDT], [HinhThuc]) VALUES (N'KH020     ', N'Tần Mạc ', N'25489655       ', N'78 Võ Văn Tần, Phường 3,Quận 3, Tp.HCM', N'09124856963    ', N'H03       ')
GO
INSERT [dbo].[LichTrinh] ([MaTuyen], [NgayDi], [NgayDen], [SoXe]) VALUES (N'T001      ', CAST(N'2016-05-12' AS Date), CAST(N'2016-05-12' AS Date), N'10B4-32435     ')
GO
INSERT [dbo].[LichTrinh] ([MaTuyen], [NgayDi], [NgayDen], [SoXe]) VALUES (N'T001      ', CAST(N'2016-05-13' AS Date), CAST(N'2016-05-13' AS Date), N'12B3-00089     ')
GO
INSERT [dbo].[LichTrinh] ([MaTuyen], [NgayDi], [NgayDen], [SoXe]) VALUES (N'T002      ', CAST(N'2016-05-12' AS Date), CAST(N'2016-05-12' AS Date), N'20B7-43211     ')
GO
INSERT [dbo].[LichTrinh] ([MaTuyen], [NgayDi], [NgayDen], [SoXe]) VALUES (N'T003      ', CAST(N'2016-05-15' AS Date), CAST(N'2016-05-15' AS Date), N'23B1-11553     ')
GO
INSERT [dbo].[LichTrinh] ([MaTuyen], [NgayDi], [NgayDen], [SoXe]) VALUES (N'T004      ', CAST(N'2016-05-16' AS Date), CAST(N'2016-05-16' AS Date), N'25B4-47832     ')
GO
INSERT [dbo].[LichTrinh] ([MaTuyen], [NgayDi], [NgayDen], [SoXe]) VALUES (N'T005      ', CAST(N'2016-05-17' AS Date), CAST(N'2016-05-17' AS Date), N'34B5-00127     ')
GO
INSERT [dbo].[LichTrinh] ([MaTuyen], [NgayDi], [NgayDen], [SoXe]) VALUES (N'T006      ', CAST(N'2016-05-17' AS Date), CAST(N'2016-05-17' AS Date), N'56B2-89110     ')
GO
INSERT [dbo].[LichTrinh] ([MaTuyen], [NgayDi], [NgayDen], [SoXe]) VALUES (N'T007      ', CAST(N'2016-05-18' AS Date), CAST(N'2016-05-18' AS Date), N'60B1-01582     ')
GO
INSERT [dbo].[LichTrinh] ([MaTuyen], [NgayDi], [NgayDen], [SoXe]) VALUES (N'T008      ', CAST(N'2016-05-19' AS Date), CAST(N'2016-05-19' AS Date), N'62B2-87987     ')
GO
INSERT [dbo].[LichTrinh] ([MaTuyen], [NgayDi], [NgayDen], [SoXe]) VALUES (N'T009      ', CAST(N'2016-05-20' AS Date), CAST(N'2016-05-20' AS Date), N'63B1-00001     ')
GO
INSERT [dbo].[LichTrinh] ([MaTuyen], [NgayDi], [NgayDen], [SoXe]) VALUES (N'T010      ', CAST(N'2016-05-21' AS Date), CAST(N'2016-05-21' AS Date), N'65B3-23874     ')
GO
INSERT [dbo].[LichTrinh] ([MaTuyen], [NgayDi], [NgayDen], [SoXe]) VALUES (N'T011      ', CAST(N'2016-05-21' AS Date), CAST(N'2016-05-21' AS Date), N'71B6-10034     ')
GO
INSERT [dbo].[LichTrinh] ([MaTuyen], [NgayDi], [NgayDen], [SoXe]) VALUES (N'T012      ', CAST(N'2016-05-22' AS Date), CAST(N'2016-05-22' AS Date), N'74B3-99331     ')
GO
INSERT [dbo].[LichTrinh] ([MaTuyen], [NgayDi], [NgayDen], [SoXe]) VALUES (N'T013      ', CAST(N'2016-05-23' AS Date), CAST(N'2016-05-23' AS Date), N'78B3-22115     ')
GO
INSERT [dbo].[LichTrinh] ([MaTuyen], [NgayDi], [NgayDen], [SoXe]) VALUES (N'T014      ', CAST(N'2016-05-24' AS Date), CAST(N'2016-05-24' AS Date), N'85B1-00003     ')
GO
INSERT [dbo].[LichTrinh] ([MaTuyen], [NgayDi], [NgayDen], [SoXe]) VALUES (N'T015      ', CAST(N'2016-05-25' AS Date), CAST(N'2016-05-25' AS Date), N'86B2-00011     ')
GO
INSERT [dbo].[LichTrinh] ([MaTuyen], [NgayDi], [NgayDen], [SoXe]) VALUES (N'T016      ', CAST(N'2016-05-26' AS Date), CAST(N'2016-05-26' AS Date), N'88B7-11703     ')
GO
INSERT [dbo].[LichTrinh] ([MaTuyen], [NgayDi], [NgayDen], [SoXe]) VALUES (N'T017      ', CAST(N'2016-05-27' AS Date), CAST(N'2016-05-28' AS Date), N'89B2-77862     ')
GO
INSERT [dbo].[LichTrinh] ([MaTuyen], [NgayDi], [NgayDen], [SoXe]) VALUES (N'T018      ', CAST(N'2016-05-28' AS Date), CAST(N'2016-05-18' AS Date), N'89B5-67874     ')
GO
INSERT [dbo].[LichTrinh] ([MaTuyen], [NgayDi], [NgayDen], [SoXe]) VALUES (N'T019      ', CAST(N'2016-05-29' AS Date), CAST(N'2016-05-29' AS Date), N'90B1-44351     ')
GO
INSERT [dbo].[LichTrinh] ([MaTuyen], [NgayDi], [NgayDen], [SoXe]) VALUES (N'T20       ', CAST(N'2016-05-30' AS Date), CAST(N'2016-05-30' AS Date), N'90B1-44351     ')
GO
INSERT [dbo].[LoaiNhanVien] ([MaLoaiNV], [TenLoai]) VALUES (N'          ', N'')
GO
INSERT [dbo].[LoaiNhanVien] ([MaLoaiNV], [TenLoai]) VALUES (N'LNV001    ', N'Quản lí')
GO
INSERT [dbo].[LoaiNhanVien] ([MaLoaiNV], [TenLoai]) VALUES (N'LNV002    ', N'Bán vé')
GO
INSERT [dbo].[LoaiNhanVien] ([MaLoaiNV], [TenLoai]) VALUES (N'LNV003    ', N'Tài xế')
GO
INSERT [dbo].[LoaiNhanVien] ([MaLoaiNV], [TenLoai]) VALUES (N'LNV004    ', N'Phụ xe')
GO
INSERT [dbo].[LoaiTK] ([MaLoaiTK], [TenLoai]) VALUES (N'LTK001    ', N'Quản Lí')
GO
INSERT [dbo].[LoaiTK] ([MaLoaiTK], [TenLoai]) VALUES (N'LTK002    ', N'Quản Lí')
GO
INSERT [dbo].[LoaiTK] ([MaLoaiTK], [TenLoai]) VALUES (N'LTK003    ', N'Nhân Viên')
GO
INSERT [dbo].[LoaiTK] ([MaLoaiTK], [TenLoai]) VALUES (N'LTK004    ', N'Nhân Viên')
GO
INSERT [dbo].[LoaiTK] ([MaLoaiTK], [TenLoai]) VALUES (N'LTK005    ', N'Quản Lí')
GO
INSERT [dbo].[LoaiTK] ([MaLoaiTK], [TenLoai]) VALUES (N'LTK006    ', N'Quản Lí')
GO
INSERT [dbo].[LoaiTK] ([MaLoaiTK], [TenLoai]) VALUES (N'LTK007    ', N'Nhân Viên')
GO
INSERT [dbo].[LoaiTK] ([MaLoaiTK], [TenLoai]) VALUES (N'LTK008    ', N'Nhân Viên')
GO
INSERT [dbo].[LoaiTK] ([MaLoaiTK], [TenLoai]) VALUES (N'LTK009    ', N'Nhân Viên')
GO
INSERT [dbo].[LoaiTK] ([MaLoaiTK], [TenLoai]) VALUES (N'LTK010    ', N'Quản Lí')
GO
INSERT [dbo].[LoaiTK] ([MaLoaiTK], [TenLoai]) VALUES (N'LTK011    ', N'Quản Lí')
GO
INSERT [dbo].[LoaiTK] ([MaLoaiTK], [TenLoai]) VALUES (N'LTK012    ', N'Nhân Viên')
GO
INSERT [dbo].[LoaiTK] ([MaLoaiTK], [TenLoai]) VALUES (N'LTK013    ', N'Nhân Viên')
GO
INSERT [dbo].[LoaiTK] ([MaLoaiTK], [TenLoai]) VALUES (N'LTK014    ', N'Nhân Viên')
GO
INSERT [dbo].[LoaiTK] ([MaLoaiTK], [TenLoai]) VALUES (N'LTK015    ', N'Nhân Viên')
GO
INSERT [dbo].[LoaiTK] ([MaLoaiTK], [TenLoai]) VALUES (N'LTK016    ', N'Quản Lí')
GO
INSERT [dbo].[LoaiTK] ([MaLoaiTK], [TenLoai]) VALUES (N'LTK017    ', N'Quản Lí')
GO
INSERT [dbo].[LoaiTK] ([MaLoaiTK], [TenLoai]) VALUES (N'LTK018    ', N'Nhân Viên')
GO
INSERT [dbo].[LoaiTK] ([MaLoaiTK], [TenLoai]) VALUES (N'LTK019    ', N'Nhân Viên')
GO
INSERT [dbo].[LoaiTK] ([MaLoaiTK], [TenLoai]) VALUES (N'LTK020    ', N'Nhân Viên')
GO
INSERT [dbo].[NhanVien] ([MaNV], [TenNV], [NgaySinh], [CMND], [DiaChi], [SDT], [LoaiNV]) VALUES (N'NV001     ', N'Dương Hoàng Lan', CAST(N'1996-11-01' AS Date), N'312011789      ', N'14 Võ Văn Kiệt Phường 5 Quận 5', N'0989812365     ', N'LNV001    ')
GO
INSERT [dbo].[NhanVien] ([MaNV], [TenNV], [NgaySinh], [CMND], [DiaChi], [SDT], [LoaiNV]) VALUES (N'NV002     ', N'Đỗ Mai Trúc', CAST(N'1998-12-03' AS Date), N'201124534      ', N'34 Nguyễn Đình Chính Phường 17 Quận Phú Nhuận', N'01232656871    ', N'LNV002    ')
GO
INSERT [dbo].[NhanVien] ([MaNV], [TenNV], [NgaySinh], [CMND], [DiaChi], [SDT], [LoaiNV]) VALUES (N'NV003     ', N'Huỳnh Văn Trí', CAST(N'2000-02-09' AS Date), N'454567761      ', N'12 Trần Bình Trọng', N'0971234567     ', N'LNV003    ')
GO
INSERT [dbo].[NhanVien] ([MaNV], [TenNV], [NgaySinh], [CMND], [DiaChi], [SDT], [LoaiNV]) VALUES (N'NV004     ', N'Lê Ngọc Linh', CAST(N'1996-02-04' AS Date), N'232327678      ', N'127 Nguyễn Văn Linh Phường 7 Quận 7', N'01298787837    ', N'LNV004    ')
GO
INSERT [dbo].[NhanVien] ([MaNV], [TenNV], [NgaySinh], [CMND], [DiaChi], [SDT], [LoaiNV]) VALUES (N'NV005     ', N'Nguyễn Minh Châu', CAST(N'1996-08-12' AS Date), N'110054879      ', N'2 Ngô Quyền Phường 2 Quận 6', N'01638987871    ', N'LNV001    ')
GO
INSERT [dbo].[NhanVien] ([MaNV], [TenNV], [NgaySinh], [CMND], [DiaChi], [SDT], [LoaiNV]) VALUES (N'NV006     ', N'Nguyễn Minh Uyên', CAST(N'1996-12-07' AS Date), N'340098982      ', N'13 Nguyễn Tri Phương Phường 12 Quận 5', N'0901212672     ', N'LNV002    ')
GO
INSERT [dbo].[NhanVien] ([MaNV], [TenNV], [NgaySinh], [CMND], [DiaChi], [SDT], [LoaiNV]) VALUES (N'NV007     ', N'Nguyễn Ngọc Hương', CAST(N'1996-02-09' AS Date), N'326767890      ', N'56 Cách Mạng Tháng Tám Phường 7 Quận 5', N'01648787897    ', N'LNV003    ')
GO
INSERT [dbo].[NhanVien] ([MaNV], [TenNV], [NgaySinh], [CMND], [DiaChi], [SDT], [LoaiNV]) VALUES (N'NV008     ', N'Nguyễn Thị Khánh', CAST(N'1996-12-09' AS Date), N'876756451      ', N'14 Quốc Hương Phường Thảo Điền Quận 2', N'0971212768     ', N'LNV004    ')
GO
INSERT [dbo].[NhanVien] ([MaNV], [TenNV], [NgaySinh], [CMND], [DiaChi], [SDT], [LoaiNV]) VALUES (N'NV009     ', N'Nguyễn Văn Phong', CAST(N'1996-10-23' AS Date), N'893465788      ', N'245 Xô Viết Nghệ Tĩnh Phường 17 Quận Bình Thạnh', N'01239878879    ', N'LNV002    ')
GO
INSERT [dbo].[NhanVien] ([MaNV], [TenNV], [NgaySinh], [CMND], [DiaChi], [SDT], [LoaiNV]) VALUES (N'NV010     ', N'Nguyễn Xuân Hoàng', CAST(N'1996-03-08' AS Date), N'008985655      ', N'322 Bùi Hữu Nghĩa Phường 22 Quận Bình Thạnh', N'0901243787     ', N'LNV003    ')
GO
INSERT [dbo].[NhanVien] ([MaNV], [TenNV], [NgaySinh], [CMND], [DiaChi], [SDT], [LoaiNV]) VALUES (N'NV011     ', N'Phạm Hoàng Dung', CAST(N'1996-10-09' AS Date), N'997867556      ', N'190 Phạm Văn Đồng Phường 10 Quận Gò Vấp', N'01202322898    ', N'LNV004    ')
GO
INSERT [dbo].[NhanVien] ([MaNV], [TenNV], [NgaySinh], [CMND], [DiaChi], [SDT], [LoaiNV]) VALUES (N'NV012     ', N'Phạm Hữu Thạnh', CAST(N'1996-02-25' AS Date), N'338898078      ', N'23 Trần Hưng Đạo Phường 5 Quận 5', N'0969878889     ', N'LNV002    ')
GO
INSERT [dbo].[NhanVien] ([MaNV], [TenNV], [NgaySinh], [CMND], [DiaChi], [SDT], [LoaiNV]) VALUES (N'NV013     ', N'Phạm Ninh Xuân', CAST(N'1996-06-07' AS Date), N'234434787      ', N'222 Phan Văn Trị Phường 10 Quận Gò Vấp', N'01232667545    ', N'LNV003    ')
GO
INSERT [dbo].[NhanVien] ([MaNV], [TenNV], [NgaySinh], [CMND], [DiaChi], [SDT], [LoaiNV]) VALUES (N'NV014     ', N'Phạm Ngọc Mai', CAST(N'1996-01-22' AS Date), N'349856557      ', N'9 Nguyễn Văn Cừ Phường 1 Quận 5', N'0987687667     ', N'LNV004    ')
GO
INSERT [dbo].[NhanVien] ([MaNV], [TenNV], [NgaySinh], [CMND], [DiaChi], [SDT], [LoaiNV]) VALUES (N'NV015     ', N'Phạm Ngọc Tuyết', CAST(N'1996-05-04' AS Date), N'343322656      ', N'78 Lê Văn Việt Phường 3 Quận 9', N'0128898667     ', N'LNV002    ')
GO
INSERT [dbo].[NhanVien] ([MaNV], [TenNV], [NgaySinh], [CMND], [DiaChi], [SDT], [LoaiNV]) VALUES (N'NV016     ', N'Trần Minh Hoàng', CAST(N'1996-03-23' AS Date), N'228787656      ', N'90 Điện Biên Phủ Phường 8 Quận Bình Thạnh', N'0909912178     ', N'LNV003    ')
GO
INSERT [dbo].[NhanVien] ([MaNV], [TenNV], [NgaySinh], [CMND], [DiaChi], [SDT], [LoaiNV]) VALUES (N'NV017     ', N'Trần Minh Thiện', CAST(N'1995-05-09' AS Date), N'783454222      ', N'100 Võ Văn Tần Phường 4 Quận 3', N'01229886667    ', N'LNV004    ')
GO
INSERT [dbo].[NhanVien] ([MaNV], [TenNV], [NgaySinh], [CMND], [DiaChi], [SDT], [LoaiNV]) VALUES (N'NV018     ', N'Trần Thiện Nhân', CAST(N'1997-02-09' AS Date), N'312276788      ', N'221 Lý Thường Kiệt Phường 17 Quận 10', N'0985567667     ', N'LNV002    ')
GO
INSERT [dbo].[NhanVien] ([MaNV], [TenNV], [NgaySinh], [CMND], [DiaChi], [SDT], [LoaiNV]) VALUES (N'NV019     ', N'Võ Minh Huyền', CAST(N'1998-04-03' AS Date), N'324434778      ', N'12 Hoàng Hoa Thám Phường 11 Quận 1', N'01237674645    ', N'LNV003    ')
GO
INSERT [dbo].[NhanVien] ([MaNV], [TenNV], [NgaySinh], [CMND], [DiaChi], [SDT], [LoaiNV]) VALUES (N'NV020     ', N'Võ Văn Trung', CAST(N'1999-08-07' AS Date), N'312245679      ', N'111 Cao Thắng Phường 11 Quận 3', N'0968988445     ', N'LNV004    ')
GO
INSERT [dbo].[PhanCong] ([MaNV], [NgayDi], [MaTuyen]) VALUES (N'NV003     ', CAST(N'2016-05-12' AS Date), N'T001      ')
GO
INSERT [dbo].[PhanCong] ([MaNV], [NgayDi], [MaTuyen]) VALUES (N'NV004     ', CAST(N'2016-05-12' AS Date), N'T001      ')
GO
INSERT [dbo].[PhanCong] ([MaNV], [NgayDi], [MaTuyen]) VALUES (N'NV007     ', CAST(N'2016-05-13' AS Date), N'T001      ')
GO
INSERT [dbo].[PhanCong] ([MaNV], [NgayDi], [MaTuyen]) VALUES (N'NV008     ', CAST(N'2016-05-16' AS Date), N'T001      ')
GO
INSERT [dbo].[PhanCong] ([MaNV], [NgayDi], [MaTuyen]) VALUES (N'NV010     ', CAST(N'2016-05-17' AS Date), N'T005      ')
GO
INSERT [dbo].[PhanCong] ([MaNV], [NgayDi], [MaTuyen]) VALUES (N'NV011     ', CAST(N'2016-05-17' AS Date), N'T005      ')
GO
INSERT [dbo].[PhanCong] ([MaNV], [NgayDi], [MaTuyen]) VALUES (N'NV013     ', CAST(N'2016-05-18' AS Date), N'T007      ')
GO
INSERT [dbo].[PhanCong] ([MaNV], [NgayDi], [MaTuyen]) VALUES (N'NV014     ', CAST(N'2016-05-18' AS Date), N'T007      ')
GO
INSERT [dbo].[PhanCong] ([MaNV], [NgayDi], [MaTuyen]) VALUES (N'NV016     ', CAST(N'2016-05-20' AS Date), N'T009      ')
GO
INSERT [dbo].[PhanCong] ([MaNV], [NgayDi], [MaTuyen]) VALUES (N'NV017     ', CAST(N'2016-05-20' AS Date), N'T009      ')
GO
INSERT [dbo].[PhanCong] ([MaNV], [NgayDi], [MaTuyen]) VALUES (N'NV019     ', CAST(N'2016-05-29' AS Date), N'T016      ')
GO
INSERT [dbo].[PhanCong] ([MaNV], [NgayDi], [MaTuyen]) VALUES (N'NV020     ', CAST(N'2016-05-29' AS Date), N'T016      ')
GO
INSERT [dbo].[TaiKhoan] ([TenDN], [MatKhau], [LoaiTK]) VALUES (N'Dương Hoàng Lan', N'887823    ', N'LTK018    ')
GO
INSERT [dbo].[TaiKhoan] ([TenDN], [MatKhau], [LoaiTK]) VALUES (N'Đỗ Mai Trúc', N'170322    ', N'LTK011    ')
GO
INSERT [dbo].[TaiKhoan] ([TenDN], [MatKhau], [LoaiTK]) VALUES (N'Huỳnh Văn Trí', N'120300    ', N'LTK014    ')
GO
INSERT [dbo].[TaiKhoan] ([TenDN], [MatKhau], [LoaiTK]) VALUES (N'Lê Ngọc Linh', N'434312    ', N'LTK004    ')
GO
INSERT [dbo].[TaiKhoan] ([TenDN], [MatKhau], [LoaiTK]) VALUES (N'Nguyễn Minh Châu', N'332349    ', N'LTK020    ')
GO
INSERT [dbo].[TaiKhoan] ([TenDN], [MatKhau], [LoaiTK]) VALUES (N'Nguyễn Minh Uyên', N'051122    ', N'LTK001    ')
GO
INSERT [dbo].[TaiKhoan] ([TenDN], [MatKhau], [LoaiTK]) VALUES (N'Nguyễn Ngọc Hương', N'332201    ', N'LTK015    ')
GO
INSERT [dbo].[TaiKhoan] ([TenDN], [MatKhau], [LoaiTK]) VALUES (N'Nguyễn Thị Khánh', N'140522    ', N'LTK013    ')
GO
INSERT [dbo].[TaiKhoan] ([TenDN], [MatKhau], [LoaiTK]) VALUES (N'Nguyễn Văn Phong', N'170214    ', N'LTK008    ')
GO
INSERT [dbo].[TaiKhoan] ([TenDN], [MatKhau], [LoaiTK]) VALUES (N'Nguyễn Xuân Hoàng', N'232323    ', N'LTK005    ')
GO
INSERT [dbo].[TaiKhoan] ([TenDN], [MatKhau], [LoaiTK]) VALUES (N'Phạm Hoàng Dung', N'019188    ', N'LTK010    ')
GO
INSERT [dbo].[TaiKhoan] ([TenDN], [MatKhau], [LoaiTK]) VALUES (N'Phạm Hữu Thạnh', N'091414    ', N'LTK009    ')
GO
INSERT [dbo].[TaiKhoan] ([TenDN], [MatKhau], [LoaiTK]) VALUES (N'Phạm Ninh Xuân', N'546777    ', N'LTK017    ')
GO
INSERT [dbo].[TaiKhoan] ([TenDN], [MatKhau], [LoaiTK]) VALUES (N'Phạm Ngọc Mai', N'908989    ', N'LTK006    ')
GO
INSERT [dbo].[TaiKhoan] ([TenDN], [MatKhau], [LoaiTK]) VALUES (N'Phạm Ngọc Tuyết', N'110199    ', N'LTK012    ')
GO
INSERT [dbo].[TaiKhoan] ([TenDN], [MatKhau], [LoaiTK]) VALUES (N'Trần Minh Hoàng', N'545567    ', N'LTK007    ')
GO
INSERT [dbo].[TaiKhoan] ([TenDN], [MatKhau], [LoaiTK]) VALUES (N'Trần Minh Thiện', N'768890    ', N'LTK016    ')
GO
INSERT [dbo].[TaiKhoan] ([TenDN], [MatKhau], [LoaiTK]) VALUES (N'Trần Thiện Nhân', N'321100    ', N'LTK003    ')
GO
INSERT [dbo].[TaiKhoan] ([TenDN], [MatKhau], [LoaiTK]) VALUES (N'Võ Minh Huyền', N'120911    ', N'LTK019    ')
GO
INSERT [dbo].[TaiKhoan] ([TenDN], [MatKhau], [LoaiTK]) VALUES (N'Võ Văn Trung', N'110011    ', N'LTK001    ')
GO
INSERT [dbo].[TuyenXe] ([MaTuyen], [DiaDiemDi], [DiaDiemDen], [GioDi], [GioDen]) VALUES (N'T001      ', N'TPHCM', N'Nha Trang', CAST(N'06:00:00' AS Time), CAST(N'10:00:00' AS Time))
GO
INSERT [dbo].[TuyenXe] ([MaTuyen], [DiaDiemDi], [DiaDiemDen], [GioDi], [GioDen]) VALUES (N'T002      ', N'Vũng Tàu', N'Ninh Thuận', CAST(N'07:00:00' AS Time), CAST(N'18:00:00' AS Time))
GO
INSERT [dbo].[TuyenXe] ([MaTuyen], [DiaDiemDi], [DiaDiemDen], [GioDi], [GioDen]) VALUES (N'T003      ', N'TPHCM', N'Tiền Giang', CAST(N'12:00:00' AS Time), CAST(N'15:00:00' AS Time))
GO
INSERT [dbo].[TuyenXe] ([MaTuyen], [DiaDiemDi], [DiaDiemDen], [GioDi], [GioDen]) VALUES (N'T004      ', N'Cà Mau', N'Bình Thuận', CAST(N'11:00:00' AS Time), CAST(N'23:00:00' AS Time))
GO
INSERT [dbo].[TuyenXe] ([MaTuyen], [DiaDiemDi], [DiaDiemDen], [GioDi], [GioDen]) VALUES (N'T005      ', N'TPHCM', N'Đồng Nai', CAST(N'08:00:00' AS Time), CAST(N'10:00:00' AS Time))
GO
INSERT [dbo].[TuyenXe] ([MaTuyen], [DiaDiemDi], [DiaDiemDen], [GioDi], [GioDen]) VALUES (N'T006      ', N'Vũng Tàu', N'TPHCM', CAST(N'01:00:00' AS Time), CAST(N'03:00:00' AS Time))
GO
INSERT [dbo].[TuyenXe] ([MaTuyen], [DiaDiemDi], [DiaDiemDen], [GioDi], [GioDen]) VALUES (N'T007      ', N'Vĩnh Long', N'TPHCM', CAST(N'03:00:00' AS Time), CAST(N'07:00:00' AS Time))
GO
INSERT [dbo].[TuyenXe] ([MaTuyen], [DiaDiemDi], [DiaDiemDen], [GioDi], [GioDen]) VALUES (N'T008      ', N'An Giang', N'Bình Thuận', CAST(N'02:00:00' AS Time), CAST(N'08:00:00' AS Time))
GO
INSERT [dbo].[TuyenXe] ([MaTuyen], [DiaDiemDi], [DiaDiemDen], [GioDi], [GioDen]) VALUES (N'T009      ', N'Hà Nội', N'TPHCM', CAST(N'01:00:00' AS Time), CAST(N'22:00:00' AS Time))
GO
INSERT [dbo].[TuyenXe] ([MaTuyen], [DiaDiemDi], [DiaDiemDen], [GioDi], [GioDen]) VALUES (N'T010      ', N'Đà Nẵng', N'Ninh Bình', CAST(N'05:00:00' AS Time), CAST(N'12:00:00' AS Time))
GO
INSERT [dbo].[TuyenXe] ([MaTuyen], [DiaDiemDi], [DiaDiemDen], [GioDi], [GioDen]) VALUES (N'T011      ', N'An Giang', N'Tiền Giang', CAST(N'20:00:00' AS Time), CAST(N'22:00:00' AS Time))
GO
INSERT [dbo].[TuyenXe] ([MaTuyen], [DiaDiemDi], [DiaDiemDen], [GioDi], [GioDen]) VALUES (N'T012      ', N'Cần Thơ', N'Vũng Tàu', CAST(N'09:00:00' AS Time), CAST(N'16:00:00' AS Time))
GO
INSERT [dbo].[TuyenXe] ([MaTuyen], [DiaDiemDi], [DiaDiemDen], [GioDi], [GioDen]) VALUES (N'T013      ', N'Tiền Giang', N'Vĩnh Long', CAST(N'02:00:00' AS Time), CAST(N'03:00:00' AS Time))
GO
INSERT [dbo].[TuyenXe] ([MaTuyen], [DiaDiemDi], [DiaDiemDen], [GioDi], [GioDen]) VALUES (N'T014      ', N'Ninh Thuận', N'Nha Trang', CAST(N'03:00:00' AS Time), CAST(N'06:00:00' AS Time))
GO
INSERT [dbo].[TuyenXe] ([MaTuyen], [DiaDiemDi], [DiaDiemDen], [GioDi], [GioDen]) VALUES (N'T015      ', N'Nha Trang', N'Ninh Thuận', CAST(N'06:00:00' AS Time), CAST(N'09:00:00' AS Time))
GO
INSERT [dbo].[TuyenXe] ([MaTuyen], [DiaDiemDi], [DiaDiemDen], [GioDi], [GioDen]) VALUES (N'T016      ', N'Quảng Nam', N'Huế', CAST(N'02:00:00' AS Time), CAST(N'19:00:00' AS Time))
GO
INSERT [dbo].[TuyenXe] ([MaTuyen], [DiaDiemDi], [DiaDiemDen], [GioDi], [GioDen]) VALUES (N'T017      ', N'SaPa', N'Phú Yên', CAST(N'18:00:00' AS Time), CAST(N'01:00:00' AS Time))
GO
INSERT [dbo].[TuyenXe] ([MaTuyen], [DiaDiemDi], [DiaDiemDen], [GioDi], [GioDen]) VALUES (N'T018      ', N'Cần Giờ', N'Long An', CAST(N'13:00:00' AS Time), CAST(N'18:00:00' AS Time))
GO
INSERT [dbo].[TuyenXe] ([MaTuyen], [DiaDiemDi], [DiaDiemDen], [GioDi], [GioDen]) VALUES (N'T019      ', N'Long An', N'Bình Định', CAST(N'16:00:00' AS Time), CAST(N'23:00:00' AS Time))
GO
INSERT [dbo].[TuyenXe] ([MaTuyen], [DiaDiemDi], [DiaDiemDen], [GioDi], [GioDen]) VALUES (N'T20       ', N'Vũng Tàu', N'Quảng Trị', CAST(N'01:00:00' AS Time), CAST(N'22:00:00' AS Time))
GO
INSERT [dbo].[VeXe] ([MaVe], [MaGhe], [MaKH], [NgayDi], [GioDi], [SoXe], [GiaVe], [NhanVienBV]) VALUES (N'V001      ', N'GH002     ', N'KH005     ', CAST(N'2017-04-12' AS Date), CAST(N'12:00:00' AS Time), N'12B3-00089     ', 50000, N'NV001     ')
GO
INSERT [dbo].[VeXe] ([MaVe], [MaGhe], [MaKH], [NgayDi], [GioDi], [SoXe], [GiaVe], [NhanVienBV]) VALUES (N'V002      ', N'GH003     ', N'KH009     ', CAST(N'2017-03-03' AS Date), CAST(N'11:00:00' AS Time), N'20B7-43211     ', 100000, N'NV002     ')
GO
INSERT [dbo].[VeXe] ([MaVe], [MaGhe], [MaKH], [NgayDi], [GioDi], [SoXe], [GiaVe], [NhanVienBV]) VALUES (N'V003      ', N'GH004     ', N'KH003     ', CAST(N'2017-04-04' AS Date), CAST(N'02:00:00' AS Time), N'23B1-11553     ', 200000, N'NV003     ')
GO
INSERT [dbo].[VeXe] ([MaVe], [MaGhe], [MaKH], [NgayDi], [GioDi], [SoXe], [GiaVe], [NhanVienBV]) VALUES (N'V004      ', N'GH005     ', N'KH001     ', CAST(N'2017-04-05' AS Date), CAST(N'09:00:00' AS Time), N'10B4-32435     ', 150000, N'NV004     ')
GO
INSERT [dbo].[VeXe] ([MaVe], [MaGhe], [MaKH], [NgayDi], [GioDi], [SoXe], [GiaVe], [NhanVienBV]) VALUES (N'V005      ', N'GH006     ', N'KH004     ', CAST(N'2017-06-07' AS Date), CAST(N'10:00:00' AS Time), N'25B4-47832     ', 300000, N'NV005     ')
GO
INSERT [dbo].[VeXe] ([MaVe], [MaGhe], [MaKH], [NgayDi], [GioDi], [SoXe], [GiaVe], [NhanVienBV]) VALUES (N'V006      ', N'GH007     ', N'KH002     ', CAST(N'2017-04-03' AS Date), CAST(N'22:00:00' AS Time), N'34B5-00127     ', 200000, N'NV006     ')
GO
INSERT [dbo].[VeXe] ([MaVe], [MaGhe], [MaKH], [NgayDi], [GioDi], [SoXe], [GiaVe], [NhanVienBV]) VALUES (N'V007      ', N'GH010     ', N'KH007     ', CAST(N'2017-05-06' AS Date), CAST(N'03:00:00' AS Time), N'56B2-89110     ', 100000, N'NV007     ')
GO
INSERT [dbo].[VeXe] ([MaVe], [MaGhe], [MaKH], [NgayDi], [GioDi], [SoXe], [GiaVe], [NhanVienBV]) VALUES (N'V008      ', N'GH020     ', N'KH008     ', CAST(N'2017-02-12' AS Date), CAST(N'08:00:00' AS Time), N'60B1-01582     ', 300000, N'NV008     ')
GO
INSERT [dbo].[VeXe] ([MaVe], [MaGhe], [MaKH], [NgayDi], [GioDi], [SoXe], [GiaVe], [NhanVienBV]) VALUES (N'V009      ', N'GH030     ', N'KH006     ', CAST(N'2017-02-03' AS Date), CAST(N'05:00:00' AS Time), N'62B2-87987     ', 500000, N'NV009     ')
GO
INSERT [dbo].[VeXe] ([MaVe], [MaGhe], [MaKH], [NgayDi], [GioDi], [SoXe], [GiaVe], [NhanVienBV]) VALUES (N'V010      ', N'GH040     ', N'KH011     ', CAST(N'2017-03-04' AS Date), CAST(N'20:00:00' AS Time), N'63B1-00001     ', 400000, N'NV010     ')
GO
INSERT [dbo].[VeXe] ([MaVe], [MaGhe], [MaKH], [NgayDi], [GioDi], [SoXe], [GiaVe], [NhanVienBV]) VALUES (N'V011      ', N'GH050     ', N'KH010     ', CAST(N'2017-08-05' AS Date), CAST(N'18:00:00' AS Time), N'65B3-23874     ', 200000, N'NV011     ')
GO
INSERT [dbo].[VeXe] ([MaVe], [MaGhe], [MaKH], [NgayDi], [GioDi], [SoXe], [GiaVe], [NhanVienBV]) VALUES (N'V012      ', N'GH060     ', N'KH013     ', CAST(N'2017-08-09' AS Date), CAST(N'13:00:00' AS Time), N'71B6-10034     ', 120000, N'NV012     ')
GO
INSERT [dbo].[VeXe] ([MaVe], [MaGhe], [MaKH], [NgayDi], [GioDi], [SoXe], [GiaVe], [NhanVienBV]) VALUES (N'V013      ', N'GH070     ', N'KH012     ', CAST(N'2017-04-05' AS Date), CAST(N'07:00:00' AS Time), N'74B3-99331     ', 150000, N'NV013     ')
GO
INSERT [dbo].[VeXe] ([MaVe], [MaGhe], [MaKH], [NgayDi], [GioDi], [SoXe], [GiaVe], [NhanVienBV]) VALUES (N'V014      ', N'GH100     ', N'KH015     ', CAST(N'2017-12-06' AS Date), CAST(N'01:00:00' AS Time), N'78B3-22115     ', 400000, N'NV014     ')
GO
INSERT [dbo].[VeXe] ([MaVe], [MaGhe], [MaKH], [NgayDi], [GioDi], [SoXe], [GiaVe], [NhanVienBV]) VALUES (N'V015      ', N'GH200     ', N'KH014     ', CAST(N'2017-11-05' AS Date), CAST(N'16:00:00' AS Time), N'85B1-00003     ', 100000, N'NV015     ')
GO
INSERT [dbo].[VeXe] ([MaVe], [MaGhe], [MaKH], [NgayDi], [GioDi], [SoXe], [GiaVe], [NhanVienBV]) VALUES (N'V016      ', N'GH300     ', N'KH020     ', CAST(N'2017-07-06' AS Date), CAST(N'05:00:00' AS Time), N'86B2-00011     ', 200000, N'NV016     ')
GO
INSERT [dbo].[VeXe] ([MaVe], [MaGhe], [MaKH], [NgayDi], [GioDi], [SoXe], [GiaVe], [NhanVienBV]) VALUES (N'V017      ', N'GH400     ', N'KH017     ', CAST(N'2017-04-05' AS Date), CAST(N'06:00:00' AS Time), N'88B7-11703     ', 300000, N'NV017     ')
GO
INSERT [dbo].[VeXe] ([MaVe], [MaGhe], [MaKH], [NgayDi], [GioDi], [SoXe], [GiaVe], [NhanVienBV]) VALUES (N'V018      ', N'GH500     ', N'KH016     ', CAST(N'2017-04-06' AS Date), CAST(N'20:00:00' AS Time), N'89B2-77862     ', 50000, N'NV018     ')
GO
INSERT [dbo].[VeXe] ([MaVe], [MaGhe], [MaKH], [NgayDi], [GioDi], [SoXe], [GiaVe], [NhanVienBV]) VALUES (N'V019      ', N'GH600     ', N'KH019     ', CAST(N'2017-04-07' AS Date), CAST(N'07:00:00' AS Time), N'89B5-67874     ', 400000, N'NV019     ')
GO
INSERT [dbo].[VeXe] ([MaVe], [MaGhe], [MaKH], [NgayDi], [GioDi], [SoXe], [GiaVe], [NhanVienBV]) VALUES (N'V020      ', N'GH130     ', N'KH018     ', CAST(N'2017-05-08' AS Date), CAST(N'01:00:00' AS Time), N'90B1-44351     ', 100000, N'NV020     ')
GO
INSERT [dbo].[XeKhach] ([SoXe], [HangSX]) VALUES (N'10B4-32435     ', N'SMART')
GO
INSERT [dbo].[XeKhach] ([SoXe], [HangSX]) VALUES (N'12B3-00089     ', N'CHEVROLET')
GO
INSERT [dbo].[XeKhach] ([SoXe], [HangSX]) VALUES (N'20B7-43211     ', N'BENTLY')
GO
INSERT [dbo].[XeKhach] ([SoXe], [HangSX]) VALUES (N'23B1-11553     ', N'JEEP')
GO
INSERT [dbo].[XeKhach] ([SoXe], [HangSX]) VALUES (N'25B4-47832     ', N'JACUAR')
GO
INSERT [dbo].[XeKhach] ([SoXe], [HangSX]) VALUES (N'34B5-00127     ', N'CODING')
GO
INSERT [dbo].[XeKhach] ([SoXe], [HangSX]) VALUES (N'56B2-89110     ', N'SUZUKI')
GO
INSERT [dbo].[XeKhach] ([SoXe], [HangSX]) VALUES (N'60B1-01582     ', N'KIA')
GO
INSERT [dbo].[XeKhach] ([SoXe], [HangSX]) VALUES (N'62B2-87987     ', N'HUYNDAI')
GO
INSERT [dbo].[XeKhach] ([SoXe], [HangSX]) VALUES (N'63B1-00001     ', N'TOYOTA')
GO
INSERT [dbo].[XeKhach] ([SoXe], [HangSX]) VALUES (N'65B3-23874     ', N'MECREDES-BENZ')
GO
INSERT [dbo].[XeKhach] ([SoXe], [HangSX]) VALUES (N'71B6-10034     ', N'HINO')
GO
INSERT [dbo].[XeKhach] ([SoXe], [HangSX]) VALUES (N'74B3-99331     ', N'GMC')
GO
INSERT [dbo].[XeKhach] ([SoXe], [HangSX]) VALUES (N'78B3-22115     ', N'OPEL')
GO
INSERT [dbo].[XeKhach] ([SoXe], [HangSX]) VALUES (N'85B1-00003     ', N'YAMAHA')
GO
INSERT [dbo].[XeKhach] ([SoXe], [HangSX]) VALUES (N'86B2-00011     ', N'AUDI')
GO
INSERT [dbo].[XeKhach] ([SoXe], [HangSX]) VALUES (N'88B7-11703     ', N'LEXUS')
GO
INSERT [dbo].[XeKhach] ([SoXe], [HangSX]) VALUES (N'89B2-77862     ', N'HONDA')
GO
INSERT [dbo].[XeKhach] ([SoXe], [HangSX]) VALUES (N'89B5-67874     ', N'BMW')
GO
INSERT [dbo].[XeKhach] ([SoXe], [HangSX]) VALUES (N'90B1-44351     ', N'FUSO')
GO
ALTER TABLE [dbo].[Ghe]  WITH CHECK ADD  CONSTRAINT [FK_Ghe_KhachHang] FOREIGN KEY([MaKH])
REFERENCES [dbo].[KhachHang] ([MaKH])
GO
ALTER TABLE [dbo].[Ghe] CHECK CONSTRAINT [FK_Ghe_KhachHang]
GO
ALTER TABLE [dbo].[Ghe]  WITH CHECK ADD  CONSTRAINT [FK_Ghe_TuyenXe] FOREIGN KEY([MaTuyen])
REFERENCES [dbo].[TuyenXe] ([MaTuyen])
GO
ALTER TABLE [dbo].[Ghe] CHECK CONSTRAINT [FK_Ghe_TuyenXe]
GO
ALTER TABLE [dbo].[HangHoa]  WITH CHECK ADD  CONSTRAINT [FK_HangHoa_NhanVien] FOREIGN KEY([NhanVienNhan])
REFERENCES [dbo].[NhanVien] ([MaNV])
GO
ALTER TABLE [dbo].[HangHoa] CHECK CONSTRAINT [FK_HangHoa_NhanVien]
GO
ALTER TABLE [dbo].[HoaDonHH]  WITH CHECK ADD  CONSTRAINT [FK_HoaDonHH_HangHoa] FOREIGN KEY([MaHH])
REFERENCES [dbo].[HangHoa] ([MaHH])
GO
ALTER TABLE [dbo].[HoaDonHH] CHECK CONSTRAINT [FK_HoaDonHH_HangHoa]
GO
ALTER TABLE [dbo].[HoaDonHH]  WITH CHECK ADD  CONSTRAINT [FK_HoaDonHH_KhachHang] FOREIGN KEY([MaKH])
REFERENCES [dbo].[KhachHang] ([MaKH])
GO
ALTER TABLE [dbo].[HoaDonHH] CHECK CONSTRAINT [FK_HoaDonHH_KhachHang]
GO
ALTER TABLE [dbo].[HoaDonVe]  WITH CHECK ADD  CONSTRAINT [FK_HoaDonVe_KhachHang] FOREIGN KEY([MaKH])
REFERENCES [dbo].[KhachHang] ([MaKH])
GO
ALTER TABLE [dbo].[HoaDonVe] CHECK CONSTRAINT [FK_HoaDonVe_KhachHang]
GO
ALTER TABLE [dbo].[HoaDonVe]  WITH CHECK ADD  CONSTRAINT [FK_HoaDonVe_VeXe] FOREIGN KEY([MaVe])
REFERENCES [dbo].[VeXe] ([MaVe])
GO
ALTER TABLE [dbo].[HoaDonVe] CHECK CONSTRAINT [FK_HoaDonVe_VeXe]
GO
ALTER TABLE [dbo].[KhachHang]  WITH CHECK ADD  CONSTRAINT [FK_KhachHang_HinhThuc] FOREIGN KEY([HinhThuc])
REFERENCES [dbo].[HinhThuc] ([LoaiHT])
GO
ALTER TABLE [dbo].[KhachHang] CHECK CONSTRAINT [FK_KhachHang_HinhThuc]
GO
ALTER TABLE [dbo].[LichTrinh]  WITH CHECK ADD  CONSTRAINT [FK_LichTrinh_TuyenXe] FOREIGN KEY([MaTuyen])
REFERENCES [dbo].[TuyenXe] ([MaTuyen])
GO
ALTER TABLE [dbo].[LichTrinh] CHECK CONSTRAINT [FK_LichTrinh_TuyenXe]
GO
ALTER TABLE [dbo].[LichTrinh]  WITH CHECK ADD  CONSTRAINT [FK_LichTrinh_TuyenXe1] FOREIGN KEY([MaTuyen])
REFERENCES [dbo].[TuyenXe] ([MaTuyen])
GO
ALTER TABLE [dbo].[LichTrinh] CHECK CONSTRAINT [FK_LichTrinh_TuyenXe1]
GO
ALTER TABLE [dbo].[NhanVien]  WITH CHECK ADD  CONSTRAINT [FK_NhanVien_LoaiNhanVien] FOREIGN KEY([LoaiNV])
REFERENCES [dbo].[LoaiNhanVien] ([MaLoaiNV])
GO
ALTER TABLE [dbo].[NhanVien] CHECK CONSTRAINT [FK_NhanVien_LoaiNhanVien]
GO
ALTER TABLE [dbo].[PhanCong]  WITH CHECK ADD  CONSTRAINT [FK_PhanCong_TuyenXe] FOREIGN KEY([MaTuyen])
REFERENCES [dbo].[TuyenXe] ([MaTuyen])
GO
ALTER TABLE [dbo].[PhanCong] CHECK CONSTRAINT [FK_PhanCong_TuyenXe]
GO
ALTER TABLE [dbo].[TaiKhoan]  WITH CHECK ADD  CONSTRAINT [FK_TaiKhoan_LoaiTK] FOREIGN KEY([LoaiTK])
REFERENCES [dbo].[LoaiTK] ([MaLoaiTK])
GO
ALTER TABLE [dbo].[TaiKhoan] CHECK CONSTRAINT [FK_TaiKhoan_LoaiTK]
GO
ALTER TABLE [dbo].[VeXe]  WITH CHECK ADD  CONSTRAINT [FK_VeXe_NhanVien] FOREIGN KEY([NhanVienBV])
REFERENCES [dbo].[NhanVien] ([MaNV])
GO
ALTER TABLE [dbo].[VeXe] CHECK CONSTRAINT [FK_VeXe_NhanVien]
GO
